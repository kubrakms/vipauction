﻿// <copyright file="UserRepositoryTestClass.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule;

using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using Ocorola.Module.Infrastructure;

namespace SoftServe.VipAuction.Testing.VipAuctionLogicModule.Test
{
    /// <summary>
    /// Test class for testing UserRepository
    /// </summary>
    [TestClass]
    public class UserRepositoryTestClass
    {
        /// <summary>
        /// Test for Add method
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            using (var userRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var etalon = new UserProfile()
                {
                    User = new User() { UserName = "TestUser" },
                    Balance = 10,
                    Discount = 1,
                    Email = "testUser@gmail.com",
                    Experience = 10,
                    FirstName = "User",
                    LastName = "testUser",
                    IsBlocked = false,
                    PoinsByMonth = 1,
                    Status = eUserStatusType.User,
                    TotalPoints = 100,
                    UserName = "TestUser",
                    BirthDate = new DateTime(1986, 05, 24)
                };

                userRepository.Add(etalon);

                var actual = userRepository.Enumerate().Where(b => b.UserId == etalon.UserId).ToArray()[0];

                Assert.AreEqual(etalon, actual);

                userRepository.Remove(actual);
            }
        }

        /// <summary>
        /// Test for Create method
        /// </summary>
        [TestMethod]
        public void CreateTest()
        {
            using (var userRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var actual = userRepository.Create();
                
                Assert.IsTrue(actual is UserProfile);

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Enumerate method
        /// </summary>
        [TestMethod]
        public void EnumerateTest()
        {
            using (var userRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var actual = userRepository.Enumerate();

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for GetQuery method
        /// </summary>
        [TestMethod]
        public void GetQueryTest()
        {
            using (var userRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var actual = userRepository.GetQuery();

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Item method
        /// </summary>
        [TestMethod]
        public void ItemTest()
        {
            using (var userRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var actual = userRepository.Item(1);

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Update method
        /// </summary>
        [TestMethod]
        public void UpdateTest()
        {
            using (var userRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var val = userRepository.Enumerate().FirstOrDefault();

                val.LastName = "testUser123";
                userRepository.Update(val);
                var actual = userRepository.Enumerate().Where(b => b.UserId == val.UserId).ToArray()[0];

                Assert.IsTrue(string.Compare(val.LastName, actual.LastName) == 0);

            }
        }

        /// <summary>
        /// Test for GetEnumerator method
        /// </summary>
        [TestMethod]
        public void GetEnumeratorTest()
        {
            using (var userRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var actual = userRepository.GetEnumerator();

                Assert.IsTrue(actual != null);
            }
        }
    }
}
