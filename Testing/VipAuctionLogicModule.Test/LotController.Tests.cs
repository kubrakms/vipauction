﻿// <copyright file="LotControllerTest.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule;

using Ocorola.Module.Infrastructure;


namespace SoftServe.VipAuction.Testing.VipAuctionLogicModule.Test
{
    /// <summary>
    /// Class for testing LotController methods
    /// </summary>
    [TestClass]
    public class LotControllerTest
    {
        /// <summary>
        /// Initialize test case
        /// </summary>
        [TestInitialize]
        public void InitTest()
        {
            preset = new List<Lot>() 
            { 
                new Lot() { Name = "test1", Description = "test1 Description", UserId = 1, State = eLotStateType.NotActivated, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test2", Description = "test2 Description", UserId = 7, State = eLotStateType.NotActivated, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test3", Description = "test3 Description", UserId = 8, State = eLotStateType.Published, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test4", Description = "test4 Description", UserId = 9, State = eLotStateType.Published, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test5", Description = "test5 Description", UserId = 9, State = eLotStateType.Canceled, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test", Description = "test6 Description", UserId = 7, State = eLotStateType.NotActivated, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test", Description = "test7 Description", UserId = 1, State = eLotStateType.Published, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test", Description = "test8 Description", UserId = 1, State = eLotStateType.NotActivated, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test", Description = "test9 Description", UserId = 1, State = eLotStateType.Published, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) },
                new Lot() { Name = "test", Description = "test0 Description", UserId = 1, State = eLotStateType.Published, StartingPrice = 100, CurrentBet = 105, LastBetUserId = 6, EndDateTime = new DateTime(2013, 12, 12) }
            };
        }

        /// <summary>
        /// Clean up test case
        /// </summary>
        [TestCleanup]
        public void CleanupTest()
        {
            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                foreach (var item in preset)
                {
                    lotRepository.Remove(item);
                }
            }
            preset.Clear();
        }

        /// <summary>
        /// Method for testing CreateLot method
        /// </summary>
        [TestMethod]
        public void CreateLotTest()
        {
            Lot actual = new Lot();

            using (var lotController = AuctionLogicContainer.Instance.GetService<ILotContract>())
            {
                foreach (var item in preset)
                {
                    lotController.CreateLot(item);
                    actual = lotController.GetLot(item.LotId);
                    Assert.AreEqual(item, actual);
                }
            }
        }

        /// <summary>
        /// Test for GetLotsByName method
        /// </summary>
        [TestMethod]
        public void GetLotsByNameTest()
        {
            string name = "test";
            List<Lot> etalon = preset.FindAll(x => x.Name == name) ;

            using (var lotController = AuctionLogicContainer.Instance.GetService<ILotContract>())
            {
                foreach (var item in preset)
                {
                    lotController.CreateLot(item);
                }
                List<Lot> actual = new List<Lot>(lotController.GetLotsByName(name));
                CollectionAssert.AreEqual(etalon, actual);
            }
        }

        /// <summary>
        /// Test for GetUserLots method
        /// </summary>
        [TestMethod]
        public void GetUserLotsTest()
        {
            int userId = 7;
            List<Lot> etalon = preset.FindAll(x => x.UserId == userId);

            using (var lotController = AuctionLogicContainer.Instance.GetService<ILotContract>())
            {
                foreach (var item in preset)
                {
                    lotController.CreateLot(item);
                }
                List<Lot> actual = new List<Lot>(lotController.GetUserLots(userId));
                CollectionAssert.AreEqual(etalon, actual);
            }
        }

        /// <summary>
        /// Test for ActivateLot method
        /// </summary>
        [TestMethod]
        public void ActivateLotTestMethod()
        {
            List<Lot> etalon = new List<Lot>();
            etalon = preset.FindAll(x => x.State == eLotStateType.NotActivated);

            using (var lotController = AuctionLogicContainer.Instance.GetService<ILotContract>())
            {
                foreach (var item in preset)
                {
                    lotController.CreateLot(item);
                }
                lotController.ActivateLot(etalon[0].LotId);
                Lot actual = lotController.GetLot(etalon[0].LotId);
                Assert.AreEqual(actual.State, eLotStateType.Published);
            }
        }

        /// <summary>
        /// Test for CancelLot method
        /// </summary>
        [TestMethod]
        public void CanselLotTest()
        {
            List<Lot> etalon = new List<Lot>();
            etalon = preset.FindAll(x => x.State == eLotStateType.NotActivated);

            using (var lotController = AuctionLogicContainer.Instance.GetService<ILotContract>())
            {
                foreach (var item in preset)
                {
                    lotController.CreateLot(item);
                }
                lotController.CanselLot(etalon[0].LotId);
                Lot actual = lotController.GetLot(etalon[0].LotId);
                Assert.AreEqual(actual.State, eLotStateType.Canceled);
            }
        }

        private List<Lot> preset = new List<Lot>();
    }
}
