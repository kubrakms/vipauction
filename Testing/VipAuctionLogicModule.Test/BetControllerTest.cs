﻿// <copyright file="BetControllerTest.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

namespace SoftServe.VipAuction.Testing.VipAuctionLogicModule.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
    using Ocorola.Module.Infrastructure;

    /// <summary>
    /// Class for testing BetController
    /// </summary>
    [TestClass]
    public class BetControllerTest
    {
        /// <summary>
        /// Initialize test case
        /// </summary>
        [TestInitialize]
        public void InitTest()
        {
            preset = new List<Bet>()
            {
                new Bet() { BetValue = 150, LotId = 7, UserId = 7, Status = eBetStatusType.Made, OpenDate = new DateTime(2013, 12, 12) },
                new Bet() { BetValue = 140, LotId = 8, UserId = 7, Status = eBetStatusType.Made, OpenDate = new DateTime(2013, 12, 12) },
                new Bet() { BetValue = 130, LotId = 9, UserId = 9, Status = eBetStatusType.Made, OpenDate = new DateTime(2013, 12, 12) },
                new Bet() { BetValue = 120, LotId = 7, UserId = 9, Status = eBetStatusType.Made, OpenDate = new DateTime(2013, 12, 12) },
                new Bet() { BetValue = 110, LotId = 8, UserId = 7, Status = eBetStatusType.Made, OpenDate = new DateTime(2013, 12, 12) }
            };
        }

        /// <summary>
        /// Clean Up test case
        /// </summary>
        [TestCleanup]
        public void CleanUpTest()
        {
            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                foreach (var item in preset)
                {
                    betRepository.Remove(item);
                }
            }
            preset.Clear();
        }

        /// <summary>
        /// Method for testing GetBetByUser method
        /// </summary>
        [TestMethod]
        public void MakeBetTest()
        {
            int lotId = preset[0].LotId;
            List<Bet> etalon = preset.FindAll(x => x.LotId == lotId);

            using (var betController = AuctionLogicContainer.Instance.GetService<IBetContract>())
            {
                foreach (var item in preset)
                {
                    betController.MakeBet(item);
                }
               
                List<Bet> actual = new List<Bet>(betController.GetBetsByLot(lotId));
                CollectionAssert.AreEqual(etalon, actual);
            }
        }

        /// <summary>
        /// Method for testing GetBetByLot method
        /// </summary>
        [TestMethod]
        public void GetBetByLotTest()
        {
            int lotId = preset[0].LotId;
            List<Bet> etalon = preset.FindAll(x => x.LotId == lotId);

            using (var betController = AuctionLogicContainer.Instance.GetService<IBetContract>())
            {
                //etalon = new List<Bet>(betController.GetBetsByLot(lotId));
                //foreach (var item in preset.FindAll(x => x.LotId == lotId))
                //{
                //    etalon.Add(item);
                //}
                foreach (var item in preset)
                {
                    betController.MakeBet(item);
                }
                List<Bet> actual = new List<Bet>(betController.GetBetsByLot(lotId));
                CollectionAssert.AreEqual(etalon, actual);
            }
        }

        /// <summary>
        /// Method for testing GetBetByUser method
        /// </summary>
        [TestMethod]
        public void GetBetsByUserTest()
        {
            int userId = preset[0].UserId;
            List<Bet> etalon = preset.FindAll(x => x.UserId == userId);

            using (var betController = AuctionLogicContainer.Instance.GetService<IBetContract>())
            {
                foreach (var item in preset)
                {
                    betController.MakeBet(item);
                }
                List<Bet> actual = new List<Bet>(betController.GetBetsByUser(userId));
                CollectionAssert.AreEqual(etalon, actual);
            }
        }

        private List<Bet> preset = new List<Bet>();
    }
}
