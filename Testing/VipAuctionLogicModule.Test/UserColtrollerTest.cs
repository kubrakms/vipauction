﻿// -----------------------------------------------------------------------
// <copyright file="UserColtrollerTest.cs" company="Soft Serve Academy">
// This code was be written by Dmitrij Kashirny
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.VipAuction.Testing.VipAuctionLogicModule.Test.Models
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
    using System;
    using System.Collections.Generic;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;


    /// <summary>
    /// UserProfile controller tests
    /// </summary>
    [TestClass]
    public class UserColtrollerTest
    {
        /// <summary>
        /// Test ViewUsers method
        /// </summary>
        [TestMethod]
        public void ViewUsersTest()
        {
            using (var userControl = SoftServe.VipAuction.Modules.VipAuctionServerModule.AuctionLogicContainer.Instance.GetService<IUserContract>())
            {
                List<UserProfile> temp = new List<UserProfile>(userControl.ViewUsers());
                CollectionAssert.AllItemsAreNotNull(temp);
            }
        }

        /// <summary>
        /// Test GetProfile method
        /// </summary>
        [TestMethod]
        public void GetProfileTest()
        {
            using (var userControl = SoftServe.VipAuction.Modules.VipAuctionServerModule.AuctionLogicContainer.Instance.GetService<IUserContract>())
            {
                UserProfile user = new UserProfile();
                user = userControl.GetProfile(1);
                Assert.IsNotNull(user);
            }
        }

        /// <summary>
        /// Test CreateUsers method
        /// </summary>
        [TestMethod]
        public void CreateUsersTest()
        {
            UserProfile user = new UserProfile() { Balance = 12, BirthDate = new DateTime(2000, 12, 12), Discount = 2, Email = "123@gmail.com", Experience = 23, FirstName = "firstName", IsBlocked = false, LastName = "lastName", PoinsByMonth = 12, Status = Modules.VipAuctionServerModule.Enums.eUserStatusType.User, TotalPoints = 123, User = new User() { UserName = "User" }, UserName = "User" };

            using (var userControl = SoftServe.VipAuction.Modules.VipAuctionServerModule.AuctionLogicContainer.Instance.GetService<IUserContract>())
            {
                UserProfile temp = new UserProfile();
                userControl.CreateUser(user);
                temp = userControl.GetProfile(user.UserId);
                Assert.AreEqual(user, temp);
            }
        }

        /// <summary>
        /// Test BlockUser method
        /// </summary>
        [TestMethod]
        public void BlockUserTest()
        {
            using (var userControl = SoftServe.VipAuction.Modules.VipAuctionServerModule.AuctionLogicContainer.Instance.GetService<IUserContract>())
            {
                UserProfile temp = new UserProfile();
                foreach (var user in userControl.ViewUsers())
                {
                    if (user != null && !user.IsBlocked)
                        temp = user;
                }
                Assert.IsFalse(temp.IsBlocked == true);
                userControl.BlockUser(temp.UserId, true);
                temp = userControl.GetProfile(temp.UserId);
                Assert.IsTrue(temp.IsBlocked == true);
                userControl.BlockUser(temp.UserId, false);
            }
        }

        /// <summary>
        /// Test ChangeUserBalance method
        /// </summary>
        [TestMethod]
        public void ChangeUserBalanceTest()
        {
            decimal value = 15m;

            using (var userControl = SoftServe.VipAuction.Modules.VipAuctionServerModule.AuctionLogicContainer.Instance.GetService<IUserContract>())
            {
                UserProfile temp = new UserProfile();
                temp = userControl.GetProfile(1);
                decimal etalon = temp.Balance + value;
                userControl.ChangeUserBalance(temp.UserId, value);
                temp = userControl.GetProfile(temp.UserId);
                Assert.IsTrue(temp.Balance == etalon);
            }
        }
    }
}
