﻿// <copyright file="LotRepositoryTestClass.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;

using SoftServe.VipAuction.Modules.VipAuctionServerModule;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using System.Collections;
using Ocorola.Module.Infrastructure;

namespace SoftServe.VipAuction.Testing.VipAuctionLogicModule.Test
{
    /// <summary>
    /// Test class for LotRepository
    /// </summary>
    [TestClass]
    public class LotRepositoryTestClass
    {
        /// <summary>
        /// Test for Add method
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                var etalon = new Lot()
                {
                     Name = "TestLot", 
                     Description = "Description of TestLot", 
                     StartingPrice = 100, 
                     CurrentBet = 101, 
                     UserId = 1, 
                     LastBetUserId = 1, 
                     State = eLotStateType.Published, 
                     EndDateTime = new DateTime(2013, 12, 12)
                };

                lotRepository.Add(etalon);

                var actual = lotRepository.Enumerate().Where(b => b.LotId == etalon.LotId).ToArray()[0];

                Assert.AreEqual(etalon, actual);

                lotRepository.Remove(actual);
            }
        }

        /// <summary>
        /// Test for Create method
        /// </summary>
        [TestMethod]
        public void CreateTest()
        {
            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                var actual = lotRepository.Create();

                Assert.IsTrue(actual is Lot);

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Enumerate method
        /// </summary>
        [TestMethod]
        public void EnumerateTest()
        {
            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                var actual = lotRepository.Enumerate();

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for GetQuery method
        /// </summary>
        [TestMethod]
        public void GetQueryTest()
        {
            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                var actual = lotRepository.GetQuery();

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Item method
        /// </summary>
        [TestMethod]
        public void ItemTest()
        {
            var etalon = new Lot()
            {
                Name = "TestLot",
                Description = "Description of TestLot",
                StartingPrice = 100,
                CurrentBet = 101,
                UserId = 1,
                LastBetUserId = 1,
                State = eLotStateType.Published,
                EndDateTime = new DateTime(2013, 12, 12)
            };

            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                lotRepository.Add(etalon);

                var actual = lotRepository.Item(etalon.LotId);

                Assert.AreEqual(actual, actual);

                lotRepository.Remove(actual as Lot);
            }
        }

        /// <summary>
        /// Test for Update method
        /// </summary>
        [TestMethod]
        public void UpdateTest()
        {
            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                var val = lotRepository.Enumerate().FirstOrDefault();

                val.Name = "TestLot123";
                lotRepository.Update(val);

                var actual = lotRepository.Enumerate().Where(b => b.LotId == val.LotId).ToArray()[0];

                Assert.IsTrue(string.Compare(val.Name, actual.Name) == 0);
            }
        }

        /// <summary>
        /// Test for GetEnumerator method
        /// </summary>
        [TestMethod]
        public void GetEnumeratorTest()
        {
            using (var lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>())
            {
                var actual = lotRepository.GetEnumerator();

                Assert.IsTrue(actual != null);
            }
        }
    }
}
