﻿// <copyright file="BetRepositoryTestClass.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule;

using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using System.Collections;
using Ocorola.Module.Infrastructure;

namespace SoftServe.VipAuction.Testing.VipAuctionLogicModule.Test
{
    /// <summary>
    /// Test class for testing BetRepository
    /// </summary>
    [TestClass]
    public class BetRepositoryTestClass
    {
        /// <summary>
        /// Test for Add method
        /// </summary>
        [TestMethod]
        public void AddTest()
        {
            var etalon = new Bet()
                    {
                        BetValue = 105,
                        LotId = 2,
                        Status = eBetStatusType.Made,
                        UserId = 1,
                        ClosedDate = new DateTime(2013, 03, 03),
                        OpenDate = new DateTime(2013, 12, 12)
                    };

            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                betRepository.Add(etalon);

                var actual = betRepository.Enumerate().Where(b => b.BetId == etalon.BetId).ToArray()[0];

                Assert.AreEqual(actual, etalon);

                betRepository.Remove(actual);
            }
        }

        /// <summary>
        /// Test for Create method
        /// </summary>
        [TestMethod]
        public void CreateTest()
        {
            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                var actual = betRepository.Create();

                Assert.IsTrue(actual is Bet);

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Enumerate method
        /// </summary>
        [TestMethod]
        public void EnumerateTest()
        {
            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                var actual = betRepository.Enumerate();

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for GetQuery method
        /// </summary>
        [TestMethod]
        public void GetQueryTest()
        {
            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                var actual = betRepository.GetQuery();

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Item method
        /// </summary>
        [TestMethod]
        public void ItemTest()
        {
            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                var actual = betRepository.Item(1);

                Assert.IsTrue(actual != null);
            }
        }

        /// <summary>
        /// Test for Update method
        /// </summary>
        [TestMethod]
        public void UpdateTest()
        {
            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                var val = betRepository.Enumerate().FirstOrDefault();

                val.BetValue = 200000m;
                betRepository.Update(val);

                var actual = betRepository.Enumerate().Where(b => b.UserId == val.UserId).ToArray()[0];

                Assert.IsTrue(actual.BetValue == actual.BetValue);
            }
        }

        /// <summary>
        /// Test for GetEnumerator method
        /// </summary>
        [TestMethod]
        public void GetEnumeratorTest()
        {
            using (var betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>())
            {
                var actual = betRepository.GetEnumerator();

                Assert.IsTrue(actual != null);
            }
        }
    }
}
