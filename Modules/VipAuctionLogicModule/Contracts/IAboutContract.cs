﻿// <copyright file="IAuctionContract.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts
{
    public interface IAboutContract : IDisposable
    {
        /// <summary>
        /// Return image-source path for About image
        /// </summary>
        /// <returns>AboutImageSourcePath</returns>
        string GetAboutImageSource();

        /// <summary>
        /// Return  text for About window
        /// </summary>
        /// <returns>AboutText</returns>
        string GetAboutText();

        /// <summary>
        /// Return Copyright text for About window
        /// </summary>
        /// <returns>Copyright</returns>
        string GetCopyright();

        /// <summary>
        /// Contact information
        /// </summary>
        /// <returns>Contact information</returns>
        string GetContacts();

        /// <summary>
        /// Return team which develop this project
        /// </summary>
        /// <returns></returns>
        IEnumerable<DevelopersInfo> GetDevelopers();

        /// <summary>
        /// Set the about storage file
        /// </summary>
        /// <param name="filePath">file path</param>
        void SetFilePath(string filePath);
    }
}