﻿// <copyright file="IBetContract.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts
{
    /// <summary>
    /// represents bet controller functionality.
    /// Everything that other modules can make with bet lies here
    /// </summary>
    public interface IBetContract : IDisposable
    {
        /// <summary>
        /// The method searches and retrievs all the bets connected with concrete lot 
        /// </summary>
        /// <param name="lotId">unique identificator of lot connected with one or many bets</param>
        /// <returns>enumeration of bets related to lot with specified Id</returns>
        IEnumerable<Bet> GetBetsByLot(int lotId);
        
        /// <summary>
        /// The method searches and retrievs all the bets connected with concrete user
        /// </summary>
        /// <param name="userId">unique identificator of user connected with the set of bets to retrieve</param>
        /// <returns>enumeration of bets made by specified User</returns>
        IEnumerable<Bet> GetBetsByUser(int userId);
        
        /// <summary>
        /// Saves new Bet into Bet's repository 
        /// </summary>
        /// <param name="bet">instance of Bet domain model class to save</param>
        void MakeBet(Bet bet);
    }
}
