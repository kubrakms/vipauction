﻿// <copyright file="ILotContract.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts
{
    public interface ILotContract : IDisposable
    {
        /// <summary>
        /// returns enumeration of all lots with state marked as published which names include specified text 
        /// </summary>
        /// <param name="searchedText">text to search in names of available lots</param>
        /// <returns>enumeration of found lots</returns>
        IEnumerable<Lot> GetAvailableLots(string searchedText = null);
        
        /// <summary>
        /// returns enumeration of all lots with state marked as NotActivated which names include specified text. These function is used by administrator
        /// </summary>
        /// <param name="searchedText">text to search in names of not activated lots</param>
        /// <returns>enumeration of found lots</returns>
        IEnumerable<Lot> GetNotActivatedLots(string searchedText = null);
        
        /// <summary>
        /// retrieves all lots marked as published and sorts it using standart Sort method of List with specified comparer
        /// </summary>
        /// <param name="comparer">Comparer to pass to List with specified condition of sorting</param>
        void SortAvailableLots(IComparer<Lot> comparer);
        
        /// <summary>
        /// search and retrievs all lots published by user with specified name
        /// </summary>
        /// <param name="lotName">name of the owner of all lots to search</param>
        /// <returns>enumeration of all lots connected with specified user</returns>
        IEnumerable<Lot> GetLotsByName(string lotName);
        
        /// <summary>
        /// among the all lots of the user with specified id retrieves only lots wich name contain specified text.
        /// </summary>
        /// <param name="userId">unique id of the user who owns the lots to find</param>
        /// <param name="searchedText">text wich contains in names of lots to find</param>
        /// <returns></returns>
        IEnumerable<Lot> GetUserLots(int userId, string searchedText = null);
        
        /// <summary>
        /// find the lot with specified id
        /// </summary>
        /// <param name="lotId">unique id of required lot</param>
        /// <returns>the instance of found Lot. Returns default(Lot) if required lot wasn't found</returns>
        Lot GetLot(int lotId);
        
        /// <summary>
        /// Saves passed lot into repository
        /// </summary>
        /// <param name="lot">instance of Lot which parameters will be assigned to a new lot in repository</param>
        void CreateLot(Lot lot);
        
        /// <summary>
        /// retrievs the Lot with specified id and changes its status from NotActivated to Canceled.
        /// Then call Update method of the corresponding repository.
        /// </summary>
        /// <param name="lotId">unique identificator of lot to be canceled</param>
        void CanselLot(int lotId);
        
        /// <summary>
        /// retrievs the Lot with specified id and changes its status from NotActivated to Published.
        /// Then call Update method of the corresponding repository.
        /// These method is available only for administrator role.
        /// </summary>
        /// <param name="lotId">unique identificator of lot to be activated</param>
        void ActivateLot(int lotId);
        
        /// <summary>
        /// retrievs the Lot with specified id and changes its status to Blocked.
        /// Then call Update method of the corresponding repository.
        /// These method is available only for administrator role.
        /// </summary>
        /// <param name="lotId">unique identificator of lot to be blocked</param>
        void BlockLot(int lotId);
    }
}
