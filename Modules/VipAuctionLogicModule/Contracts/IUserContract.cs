﻿// <copyright file="IUserContract.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts
{
    /// <summary>
    /// Represant user logic
    /// </summary>
    public interface IUserContract : IDisposable
    {
        /// <summary>
        /// Retrievs the enumeration of users from the corresponding repository
        /// which first names or last names or logins contains the specified text.
        /// If the text is not passed, returns all users.
        /// </summary>
        /// <param name="searchedText">The text which will be searched in the properties listed above</param>
        /// <returns>Enumeration of UserProfile instances of found users</returns>
        IEnumerable<UserProfile> ViewUsers(string searchedText = null);
       
        /// <summary>
        /// Retrives the user from the corresponding repository with specified id
        /// </summary>
        /// <param name="userId">unique identificator of the requierd user</param>
        /// <returns>UserProfile instance of the user retrived by specified id</returns>
        UserProfile GetProfile(int userId);
        
        /// <summary>
        /// Saves passed user to the corresponding repository
        /// </summary>
        /// <param name="userProfile">UserProfile instance which parameter will be assigned to a new user in repository</param>
        void CreateUser(UserProfile userProfile);
        
        /// <summary>
        /// Calls GetProfile method to retrive needed user by specified id from users repository 
        /// Then changes his status to blocked or unblocked depending on parameter needBlock.
        /// Then updates corresponding repository. 
        /// This method is available only for administrator's role
        /// </summary>
        /// <param name="userId">the unique identificator of user to be block or unblock</param>
        /// <param name="needBlock">the state of user which means if it will be blocked or unblocked after this method's invocation</param>
        void BlockUser(int userId, bool needBlock);
        
        /// <summary>
        /// Calls GetProfile method to retrive needed user by specified id from users repository 
        /// Then adds the specified value to his ballance.
        /// Then updates corresponding repository. 
        /// </summary>
        /// <param name="userId">the unique identificator of user which ballance will be changed</param>
        /// <param name="money">the value wich will be added to current user's ballance</param>
        void ChangeUserBalance(int userId, decimal money);
        
        /// <summary>
        /// Save all changed parameters of passed user to the repository
        /// </summary>
        /// <param name="profile">UserProfile instance needed for saving</param>
        void SaveProfile(UserProfile profile);
    }
}
