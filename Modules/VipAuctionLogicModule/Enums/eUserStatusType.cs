﻿// <copyright file="Lot.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums
{
    /// User's status. There are four different statuses:
    /// 1) Not activated - guest registered but didn't activated his e-mail yet.
    /// 2) User - guest that registered and activated his e-mail. 
    /// 3) Clubman - user who has received 1000 points
    /// 4) Veteran - user that spent over 10 years in the club.
    public enum eUserStatusType : byte
    {
        NotActivated = 0,
        User = 1,
        Clubman = 2,
        Veteran = 3
    }
}
