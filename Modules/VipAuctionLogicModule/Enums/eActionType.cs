﻿// <copyright file="eActionType.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums
{
    /// <summary>
    /// Action type:
    /// Registstrate - rise when user was register in system;
    /// Login - rise when user login in system
    /// Logout - rise when user logou from system 
    /// CreateLot - rise when user create lot
    /// CancelLot - rise when user cancel lot
    /// ChangeStatus rise when user status was changed
    /// MadeBet - rise when user made bet
    /// BalanceChanged - rise when user's balance was chenged
    /// ApplyLot - rise when administrator aprove lot
    /// BlockLot - rise when administrator reject lot
    /// BlockUser - rise when admin block user
    /// UnblockUser - rise when admin Unblock user
    /// </summary>
    public enum eActionType: byte
    {
        Register = 0,
        Login = 1,
        Logout = 2,
        CreateLot = 3,
        CancelLot = 4,
        ChangeStatus = 5,
        MadeBet = 6,
        BalanceChanged = 7,
        ApplyLot = 8,
        BlockLot = 9,
        BlockUser = 10,
        UnblockUser = 11
    }
}
