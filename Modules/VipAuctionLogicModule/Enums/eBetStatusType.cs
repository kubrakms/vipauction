﻿// <copyright file="eBetStatusType.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums
{
    /// <summary>
    /// Bet status. There are three statuses:
    /// 1) Made - user made the bet
    /// 2) Broken - someone broke this bet
    /// 3) Won - bet is winning
    /// </summary>
    public enum eBetStatusType: byte
    {
        Made = 0,
        Broken = 1,
        Won = 2
    }
}
