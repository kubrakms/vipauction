﻿// <copyright file="eLotStateType.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums
{
    /// <summary>
    /// Lot state. There are 6 states:
    /// 1) Not activated - lot is being moderated by moderator.
    /// 2) Blocked - lot was blocked by administrator.
    /// 3) Published - lot was accepted by administrator and published.
    /// 4) Canceled - lot was canceled by user. Lot can be cancelled during the first 10 minutes.
    /// 5) Sold - lot was bought by some user (it was sold out).
    /// 6) Closed - lot was closed due to the lapse of time.
    /// </summary>
    public enum eLotStateType: byte
    {
        NotActivated = 0,
        Blocked = 1,
        Published = 2,
        Canceled = 3,
        Sold = 4,
        Closed = 5
    }
}
