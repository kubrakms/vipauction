﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Ocorola.Module.Infrastructure;
using Ocorola.Module.Infrastructure.Abstractions;
using Ocorola.Module.Infrastructure.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Controllers;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Repositories;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule
{
    public class AuctionLogicContainer : IServiceProviderExt
    {

        #region [Constructors]

        private AuctionLogicContainer()
        {

        }

        #endregion // [Constructors]

        #region [Method]

        public void Initialize()
        {
            if (IsInitialized)
                return;

            mContainer.RegisterType<IUserContract, UserController>();
            mContainer.RegisterType<ILotContract, LotController>();
            mContainer.RegisterType<IBetContract, BetController>();
            mContainer.RegisterType<IUserContract, UserController>();

            mContainer.RegisterType(typeof(IRepository<>), typeof(DataBaseRepository<>));

            IsInitialized = true;
        }

        #endregion // [Method]

        #region [IServiceProviderExt Implementation]

        public T GetService<T>(object[] parameters)
        {
            return mContainer.Resolve<T>(parameters[0].ToString());
        }

        public T GetService<T>()
        {
            return mContainer.Resolve<T>();
        }

        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        #endregion  // [IServiceProviderExt Implementation]

        #region [Properties]

        public static AuctionLogicContainer Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (mInstance == null)
                    {
                        mInstance = new AuctionLogicContainer();
                        mInstance.Initialize();
                    }
                    return mInstance;
                }
            }
        }

        public bool IsInitialized { get; private set; }

        #endregion  [Properties]

        #region [Fields]

        private static readonly object __mutex = new object();
        private readonly IUnityContainer mContainer = new UnityContainer();
        private static AuctionLogicContainer mInstance = default(AuctionLogicContainer);

        #endregion //[Fields]
    }
}
