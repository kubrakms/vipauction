﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ocorola.Entity.Infrastructure;
using Ocorola.Module.Infrastructure;
using Ocorola.Module.Infrastructure.Controllers;
using Ocorola.Module.Infrastructure.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Repositories
{
    internal class DataBaseRepository<DataType> : RepositoryBase<DataType>
         where DataType : DomainModelBase, IEquatable<DataType>, new()
    {
        #region [RepositoryBase Implementation]

        protected override void Initialize()
        {
        }

        public override DataType Create()
        {
            return DataSet.Create();
        }

        public override IStorageEntity Item(object key)
        {
            return DataSet.Find(key);
        }

        public override void Add(DataType item)
        {
            if (DataSet == null) return;
            if (item == null) return;

            DataSet.Add(item);


            VipAuctionContext.Instance.SaveChanges();
        }

        public override void Update(DataType storageEntity)
        {
            DataType res = DataSet.AsEnumerable<DataType>().Where(i => i.IsKeyEquals(storageEntity)).FirstOrDefault();

            if (res == null)
                return;

            res = storageEntity;
        }

        public override void Remove(DataType entity)
        {
            DataSet.Remove(entity);

            VipAuctionContext.Instance.SaveChanges();
        }

        protected override void Dispose(bool disposeManaged)
        {
            if (!disposed)
            {
                if (disposeManaged)
                {
                    DataSet = null;
                }
            }

            disposed = true;
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        protected override DbSet<DataType> DataSet
        {
            get { return dataSet; }
            set { dataSet = value; }
        }

        #endregion // [IRepository Implementation]

        #region  [Fields engagement at the data level]

        private bool disposed = false;
        private DbSet<DataType> dataSet = VipAuctionContext.Instance.Set<DataType>();

        #endregion //[Fields engagement at the object level]

    }
}
