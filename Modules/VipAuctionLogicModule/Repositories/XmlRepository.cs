﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Ocorola.Module.Infrastructure.Models;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Repositories
{
    internal class XmlRepository<DataType> : RepositoryBase<DataType>
         where DataType : DomainModelBase, IEquatable<DataType>, new()
    {
        private const string DEFAULT_FILE_NAME = "AboutInfo.xml";

        public override void Add(DataType item)
        {
            if (DataSet == null) return;
            if (item == null) return;

            (DataSet as List<DataType>).Add(item);
            //may be add save operation
        }

        public override DataType Create()
        {
            return new DataType();
        }

        public override Ocorola.Entity.Infrastructure.IStorageEntity Item(object key)
        {
            return (DataSet as List<DataType>).SingleOrDefault(i => i.IsKeyEquals(key));
        }

        public override void Remove(DataType entity)
        {
            (DataSet as List<DataType>).Remove(entity);
            //may be add save operation
        }

        public override void Update(DataType storageEntity)
        {
            DataType res = DataSet.AsEnumerable<DataType>().Where(i => i.IsKeyEquals(storageEntity)).FirstOrDefault();

            if (res == null) return;
            res.AssignTo(storageEntity);
           // res = storageEntity;
        }

        public override bool IsReadOnly
        {
            get { return true; }
        }

        protected override void Dispose(bool disposeManaged)
        {
            if (!disposed)
            {
                if (disposeManaged)
                {
                }
            }

            disposed = true;
        }

        protected override void Initialize()
        {
            DataSet = new List<DataType>();
            var assembly= Assembly.GetExecutingAssembly();
            using (Stream sr =assembly.GetManifestResourceStream("SoftServe.VipAuction.Modules.VipAuctionServerModule.AboutInfo.xml"))
            {
                Serialization.XMLSerializer serializser = new Serialization.XMLSerializer();
                DataType resultData = (DataType)serializser.Deserialize(sr);
                (DataSet as List<DataType>).Add(resultData);
            }
        }

        #region  [Fields engagement at the data level]

        private bool disposed = false;
        private IEnumerable<DataType> dataSet = default(List<DataType>);
        #endregion //[Fields engagement at the object level]

        protected override IEnumerable<DataType> DataSet
        {
            get
            {
                return dataSet;
            }
            set
            {
                dataSet = value;
            }
        }
    }
}
