﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Ocorola.Module.Infrastructure;
using Ocorola.Module.Infrastructure.Controllers;
using Ocorola.Module.Infrastructure.Models;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Repositories
{
    internal abstract class RepositoryBase<DataType> : ControllerBase, IRepository<DataType>, IEnumerable<DataType>, IEnumerable, IDisposable
          where DataType : DomainModelBase, IEquatable<DataType>, new()
    {
        public RepositoryBase()
        {
            Initialize();
        }

        #region [IRepository Implementation]

        public abstract void Add(DataType item);

        public abstract DataType Create();
       
        public abstract Ocorola.Entity.Infrastructure.IStorageEntity Item(object key);
       
        public abstract void Remove(DataType entity);
       
        public abstract  void Update(DataType storageEntity);


        public IEnumerable<DataType> Enumerate()
        {
            return DataSet.AsEnumerable<DataType>();
        }

        public IQueryable<DataType> GetQuery()
        {
            return DataSet.AsQueryable<DataType>();
        }

        public IEnumerator<DataType> GetEnumerator()
        {
            foreach (DataType data in DataSet)
                yield return data;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (DataType data in DataSet)
                yield return data;
        }

        #endregion // [IRepository Implementation]

        #region [Properties]

        public abstract bool IsReadOnly
        {
            get;
        }

        protected abstract DbSet<DataType> DataSet
        {
            get;
            set;
        }   

        #endregion // [Properties]

    }
}
