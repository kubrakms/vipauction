﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Ocorola.Module.Infrastructure.Models;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Models
{
    /// <summary>
    /// Represent model of project about information
    /// </summary>
    [Serializable]
    [XmlRoot("AboutInfo")]
    public class AboutInformation : DomainModelBase, IEquatable<AboutInformation>
    {

        #region [DomainModelBase ovveriding]

        public override void AssignTo(object obj)
        {
            if (obj is AboutInformation)
            {
                var about = obj as AboutInformation;

                about.AboutImageSource = AboutImageSource;
                about.AboutText = AboutText;
                about.Contacts = Contacts;
                about.CopyRight = CopyRight;
                about.Develpers = new List<DevelopersInfo>(Develpers);
                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals(object key)
        {
            //if (this.Equals(key as AboutInformation))
            //{
            //    return true;
            //}
            return true;
            //bool result = true;
            //try
            //{
            //    Int32 intKey = Convert.ToInt32(key);
            //    result = intKey == Check;
            //}
            //catch
            //{
            //    result = false;
            //}

            //return result;
        }

        #endregion // [DomainModelBase ovveriding]

        #region [IEquatable<AboutInformation> Implementation]

        public bool Equals(AboutInformation other)
        {
            if (string.Compare(this.AboutImageSource, other.AboutImageSource) != 0) return false;
            if (string.Compare(this.AboutText, other.AboutText) != 0) return false;
            if (string.Compare(this.Contacts, other.Contacts) != 0) return false;
            if (string.Compare(this.CopyRight, other.CopyRight) != 0) return false;
            if (this.Develpers.Count() != other.Develpers.Count()) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            AboutInformation other = obj as AboutInformation;

            if (string.Compare(this.AboutImageSource, other.AboutImageSource) != 0) return false;
            if (string.Compare(this.AboutText, other.AboutText) != 0) return false;
            if (string.Compare(this.Contacts, other.Contacts) != 0) return false;
            if (string.Compare(this.CopyRight, other.CopyRight) != 0) return false;
            if (this.Develpers.Count() != other.Develpers.Count()) return false;
            return true; 
        }

        #endregion // [IEquatable<AboutInformation> Implementation]

        #region [Properties]

        [XmlElement("CheckId")]
        public int Check { get; set; }

        /// <summary>
        /// About image path
        /// </summary>
        [XmlElement("AboutImageSource")]
        public string AboutImageSource { get; set; }

        /// <summary>
        /// About Text
        /// </summary>
        [XmlElement("AboutText")]
        public string AboutText { get; set; }

        /// <summary>
        /// Copyright text
        /// </summary>
        [XmlElement("CopyRight")]
        public string CopyRight { get; set; }

        /// <summary>
        /// Contacts
        /// </summary>
        [XmlElement("Contacts")]
        public string Contacts { get; set; }

        /// <summary>
        /// Developers team info
        /// </summary>
        [XmlArray("Developers")]
        [XmlArrayItem("Developer")]
        public List<DevelopersInfo> Develpers { get; set; }

        [XmlIgnore]
        public string DefaultFileName { get; set; }

        #endregion // [Properties]
    }

    /// <summary>
    /// Represent Developer inforamation
    /// </summary>
    [Serializable]
    public class DevelopersInfo
    {
        /// <summary>
        /// Path to developers image
        /// </summary>
        [XmlAttribute("ImageSource")]
        public string ImageSource { get; set; }

        /// <summary>
        /// Some info about developer
        /// </summary>
        [XmlAttribute("ShortInfo")]
        public string ShortInfo { get; set; }

        /// <summary>
        /// Developer FirstName 
        /// </summary>
        [XmlAttribute("FirstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Developer LastName
        /// </summary>
        [XmlAttribute("LastName")]
        public string LastName { get; set; }
    }
}
