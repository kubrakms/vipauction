﻿// <copyright file="eActionType.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>


using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using Ocorola.Module.Infrastructure.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Models
{
    /// <summary>
    /// Describes user history.
    /// </summary>
    [Serializable]
    public class UserHistory : DomainModelBase, IEquatable<UserHistory>
    {
        #region Constructor

        public UserHistory()
        {
            UserId = default(Int32);
            ActionDate = DateTime.Now;
            Description = default(String);
        }

        #endregion

        #region Properties

        [XmlAttribute("Id")]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Id of the user who made action.
        /// </summary>
        [XmlAttribute("UserId")]
        [ForeignKey("Users")]
        public Int32 UserId { get; set; }

        public User Users { get; set; }

        /// <summary>
        /// Action type:
        /// Registstrate - rise when user was register in system;
        /// Login - rise when user login in system
        /// Logout - rise when user logou from system 
        /// CreateLot - rise when user create lot
        /// CancelLot - rise when user cancel lot
        /// ChangeStatus rise when user status was changed
        /// MadeBet - rise when user made bet
        /// BalanceChanged - rise when user's balance was chenged
        /// ApplyLot - rise when administrator aprove lot
        /// BlockLot - rise when administrator reject lot
        /// BlockUser - rise when admin block user
        /// UnblockUser - rise when admin Unblock user
        /// </summary>
        [XmlAttribute("Action")]
        public eActionType Action { get; set; }

        /// <summary>
        /// The date when the event occurred.
        /// </summary>
        [XmlAttribute("ActionDate", DataType = "date")]
        public DateTime ActionDate { get; set; }

        /// <summary>
        /// Action description.
        /// </summary>
        [XmlAttribute("Description")]
        public String Description { get; set; }

        #endregion

        #region [IStorageEntity Implementation]
        public override bool IsKeyEquals(object key)
        {
            var instance = key as UserHistory;

            if (instance != null)
            {
                if (this.UserId == instance.UserId && this.Id == instance.Id)
                    return true;
            }

            return false;
        }

        public override void AssignTo(object obj)
        {
            if (obj is UserHistory)
            {
                var userHistory = obj as UserHistory;
                userHistory.UserId = UserId;
                userHistory.Action = Action;
                userHistory.ActionDate = ActionDate;
                userHistory.Description = Description;
                return;
            }
            throw new NotSupportedException();
        }
        #endregion // [IStorageEntity Implementation]

        #region [IEquatable<Lot> Members]

        /// <summary>
        /// Check Equals objects
        /// </summary>
        /// <param name="other"><UserHistory>object</param>
        /// <returns>bool</returns>
        public bool Equals(UserHistory other)
        {
            if (other == null)
                return false;

            return (this.UserId == other.UserId)
                && (this.Description == other.Description)
                && (this.Action == other.Action)
                && (this.ActionDate == other.ActionDate);
        }

        /// <summary>
        /// Override GetHashCode method
        /// </summary>
        /// <returns>Hash Code</returns>
        public override int GetHashCode()
        {
            return UserId ^ UserId;
        }

        public override bool Equals(object b)
        {
            if (b == null)
            {
                return false;
            }

            UserHistory other = b as UserHistory;

            return (this.UserId == other.UserId)
                && (this.Description == other.Description)
                && (this.Action == other.Action)
                && (this.ActionDate == other.ActionDate);
        }

        #endregion //[IEquatable<Lot> Members]
    }
}
