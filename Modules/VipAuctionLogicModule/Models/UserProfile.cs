﻿// <copyright file="User.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>


using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using Ocorola.Module.Infrastructure.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Models
{
    /// <summary>
    /// Describes user of Auction.
    /// </summary>
    [Serializable]
    [Table("UserProfile")]
    public class UserProfile : DomainModelBase, IEquatable<UserProfile>
    {
        #region Constructor


        public UserProfile()
        {
            UserId = default(Int32);
            UserName = default(string);
            FirstName = default(String);
            LastName = default(String);
            Email = default(string);
            Status = default(eUserStatusType);
            Balance = default(Decimal);
            IsBlocked = false;
            PoinsByMonth = default(Int32);
            TotalPoints = default(Int32);
            Discount = default(Int32);
            Experience = default(Int32);
        }

        #endregion

        #region Properties

        /// <summary>
        /// User's identifier 
        /// </summary>
        [XmlAttribute("UserId")]
        [Key]
        [ForeignKey("User")]
        public Int32 UserId { get; set; }

        public User User { get; set; }


        public string UserName { get; set; }


        /// <summary>
        /// User's first name
        /// </summary>
        [XmlAttribute("FirstName")]
        public String FirstName { get; set; }

        /// <summary>
        /// User's last name
        /// </summary>
        [XmlAttribute("LastName")]
        public String LastName { get; set; }

        /// <summary>
        /// User's birthDate
        /// </summary>
        [XmlAttribute("BirthDate", DataType = "date")]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// User's e-mail
        /// </summary>
        [XmlAttribute("Email")]
        public String Email { get; set; }

        /// <summary>
        /// User's status. There are four different statuses:
        /// 1) Not activated - guest registered but didn't activated his e-mail yet.
        /// 2) User - guest that registered and activated his e-mail. 
        /// 3) Clubman - user who has received 1000 points
        /// 4) Veteran - user that spent over 10 years in the club.
        /// </summary>
        [XmlAttribute("Status")]
        public eUserStatusType Status { get; set; }

        /// <summary>
        /// User's balance in USD
        /// </summary>
        [XmlAttribute("Balance")]
        public Decimal Balance { get; set; }

        /// <summary>
        /// Shows if the user is blocked (true) or not (false).
        /// </summary>
        [XmlAttribute("IsBlocked")]
        public Boolean IsBlocked { get; set; }

        /// <summary>
        /// User's points received in the club for the month.
        /// For every success lot user gets 1% of lot's cost.
        /// 1 point = $1
        /// </summary>
        [XmlAttribute("PoinsByMonth")]
        public Int32 PoinsByMonth { get; set; }

        /// <summary>
        /// Total user's points
        /// </summary>
        [XmlAttribute("TotalPoints")]
        public Int32 TotalPoints { get; set; }

        /// <summary>
        /// User's lot cost discount.
        /// Every year spent as the club member brings user 1% of discount.
        /// Maximal discount value is 20%.
        /// </summary>
        [XmlAttribute("Discount")]
        public Byte Discount { get; set; }

        /// <summary>
        /// User's experience as the club member in months.
        /// </summary>
        [XmlAttribute("Experience")]
        public Int32 Experience { get; set; }

        #endregion

        #region [IStorageEntity Implementation]

        public override bool IsKeyEquals(object key)
        {
            var instance = key as UserProfile;

            if (instance != null)
            {
                if (this.UserId == instance.UserId)
                    return true;
            }

            return false;
        }

        public override void AssignTo(object obj)
        {
            if (obj is UserProfile)
            {
                var user = obj as UserProfile;
                user.UserId = UserId;
                user.UserName = UserName;
                user.Balance = Balance;
                user.BirthDate = BirthDate;
                user.Discount = Discount;
                user.Email = Email;
                user.Experience = Experience;
                user.FirstName = FirstName;
                user.IsBlocked = IsBlocked;
                user.LastName = LastName;
                user.PoinsByMonth = PoinsByMonth;
                user.Status = Status;
                user.TotalPoints = TotalPoints;
                return;
            }
            throw new NotSupportedException();
        }
        #endregion // [IStorageEntity Implementation]

        #region IEquatable<UserProfile> Members

        /// <summary>
        /// Check Equals objects
        /// </summary>
        /// <param name="other"><UserProfile>object</param>
        /// <returns>bool</returns>
        public bool Equals(UserProfile other)
        {
            if (other == null)
            {
                return false;
            }

            return (this.UserId == other.UserId) && (this.UserName == other.UserName)
                && (this.TotalPoints == other.TotalPoints) && (this.Status == other.Status)
                && (this.PoinsByMonth == other.PoinsByMonth) && (this.LastName == other.LastName)
                && (this.IsBlocked == other.IsBlocked) && (this.FirstName == other.FirstName)
                && (this.Experience == other.Experience) && (this.Email == other.Email)
                && (this.Discount == other.Discount) && (this.BirthDate == other.BirthDate)
                && (this.Balance == other.Balance);
        }

        /// <summary>
        /// Override GetHashCode method
        /// </summary>
        /// <returns>Hash Code</returns>
        public override int GetHashCode()
        {
            return UserId;
        }

        /// <summary>
        /// Override Equals method
        /// </summary>
        /// <param name="b"><UserProfile>object</param>
        /// <returns></returns>
        public override bool Equals(object b)
        {
            if (b == null)
            {
                return false;
            }

            UserProfile other = b as UserProfile;

            return (this.UserId == other.UserId) && (this.UserName == other.UserName)
                && (this.TotalPoints == other.TotalPoints) && (this.Status == other.Status)
                && (this.PoinsByMonth == other.PoinsByMonth) && (this.LastName == other.LastName)
                && (this.IsBlocked == other.IsBlocked) && (this.FirstName == other.FirstName)
                && (this.Experience == other.Experience) && (this.Email == other.Email)
                && (this.Discount == other.Discount) && (this.BirthDate == other.BirthDate)
                && (this.Balance == other.Balance);
        }

        #endregion
    }
}
