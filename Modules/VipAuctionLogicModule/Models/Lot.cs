﻿// <copyright file="Lot.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Ocorola.Entity.Infrastructure;
using Ocorola.Module.Infrastructure.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Models
{
    /// <summary>
    /// Model that describes auction lots.
    /// </summary>
    [Serializable]
    [Table("LotProfile")]
    public class Lot : DomainModelBase, IEquatable<Lot>
    {
        #region Constants

        private const Decimal DEFAULT_STARTING_PRICE = 1m;
        private const Decimal DEFAULT_CURRENT_BET = 1m;
        private const byte DEFAULT_LOT_LIFETIME = 3;

        #endregion

        #region Constructor

        public Lot()
        {
            LotId = default(Int32);
            UserId = default(Int32);
            Name = default(String);
            Description = default(String);
            StartingPrice = DEFAULT_STARTING_PRICE;
            CurrentBet = DEFAULT_CURRENT_BET;
            EndDateTime = DateTime.Now.AddMonths(DEFAULT_LOT_LIFETIME).Date;
            State = default(eLotStateType);
            LastBetUserId = 0;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Lot id.
        /// </summary>

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [XmlAttribute("LotId")]
        public Int32 LotId { get; set; }

        /// <summary>
        /// Club member id that published the lot.
        /// </summary>
        [XmlAttribute("UserId")]
        [ForeignKey("User")]
        public Int32 UserId { get; set; }
        public User User { get; set; }


        /// <summary>
        /// Name of the lot.
        /// </summary>
        [XmlAttribute("Name")]
        public String Name { get; set; }

        /// <summary>
        /// Description of the lot (optional).
        /// </summary>
        [XmlAttribute("Description")]
        public String Description { get; set; }

        /// <summary>
        /// Starting lot price in USD.
        /// </summary>
        [XmlAttribute("StartingPrice")]
        public Decimal StartingPrice { get; set; }

        /// <summary>
        /// Maximal bet at the moment in USD.
        /// </summary>
        [XmlAttribute("CurrentBet")]
        public Decimal CurrentBet { get; set; }

        /// <summary>
        /// Date and time of lot ending.
        /// </summary>
        [XmlAttribute("EndDateTime", DataType = "date")]
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Lot state. There are 6 states:
        /// 1) Not activated - lot is being moderated by moderator.
        /// 2) Blocked - lot was blocked by administrator.
        /// 3) Published - lot was accepted by administrator and published.
        /// 4) Canceled - lot was canceled by user. Lot can be cancelled during the first 10 minutes.
        /// 5) Sold - lot was bought by some user (it was sold out).
        /// 6) Closed - lot was closed due to the lapse of time.
        /// </summary>
        [XmlAttribute("State")]
        public eLotStateType State { get; set; }

        /// <summary>
        /// Id of the user that made last bet.
        /// </summary>
        [XmlAttribute("LastBetUserId")]
        public Int32 LastBetUserId { get; set; }

        #endregion

        #region [IStorageEntity Implementation]

        public override bool IsKeyEquals(object key)
        {
            var instance = key as Lot;

            if (instance != null)
            {
                if (this.UserId == instance.UserId && this.LotId == instance.LotId)
                {
                    return true;
                }
            }

            return false;
        }

        public override void AssignTo(object obj)
        {
            if (obj is Lot)
            {
                var lot = obj as Lot;
                lot.LotId = LotId;
                lot.CurrentBet = CurrentBet;
                lot.Description = Description;
                lot.EndDateTime = EndDateTime;
                lot.LastBetUserId = LastBetUserId;
                lot.Name = Name;
                lot.StartingPrice = StartingPrice;
                lot.State = State;
                lot.UserId = UserId;

                return;
            }
            throw new NotSupportedException();
        }
        #endregion // [IStorageEntity Implementation]

        #region IEquatable<Lot> Members

        /// <summary>
        /// Check Equals objects
        /// </summary>
        /// <param name="other"><Lot>object</param>
        /// <returns>bool</returns>
        public bool Equals(Lot other)
        {
            if (other == null)
            {
                return false;
            }

            return (this.LotId == other.LotId) && (this.UserId == other.UserId)
                && (this.Name == other.Name) && (this.Description == other.Description)
                && (this.StartingPrice == other.StartingPrice) && (this.CurrentBet == other.CurrentBet)
                && (this.EndDateTime == other.EndDateTime) && (this.State == other.State)
                && (this.LastBetUserId == other.LastBetUserId);
        }

        /// <summary>
        /// Override GetHashCode method
        /// </summary>
        /// <returns>Hash Code</returns>
        public override int GetHashCode()
        {
            return LotId ^ UserId;
        }

        /// <summary>
        /// Override Equals method
        /// </summary>
        /// <param name="b"><Lot>object</param>
        /// <returns></returns>
        public override bool Equals(object b)
        {
            if (b == null)
            {
                return false;
            }

            Lot other = b as Lot;

            return (this.LotId == other.LotId) && (this.UserId == other.UserId)
                && (this.Name == other.Name) && (this.Description == other.Description)
                && (this.StartingPrice == other.StartingPrice) && (this.CurrentBet == other.CurrentBet)
                && (this.EndDateTime == other.EndDateTime) && (this.State == other.State)
                && (this.LastBetUserId == other.LastBetUserId);
        }

        #endregion
    }
}
