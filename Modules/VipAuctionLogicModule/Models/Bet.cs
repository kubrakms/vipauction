﻿// <copyright file="Bet.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>
// <date>21.03.2013 0:54:51</date>

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Ocorola.Entity.Infrastructure;
using Ocorola.Module.Infrastructure.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Models
{
    /// <summary>
    /// Model that describes bet made by user on some lot.
    /// </summary>
    [Serializable]
    [Table("BetHistory")]
    public class Bet : DomainModelBase, IEquatable<Bet>
    {
        #region Constructor

        public Bet()
        {
            LotId = default(Int32);
            UserId = default(Int32);
            BetValue = default(Decimal);
            Status = default(eBetStatusType);
            OpenDate = DateTime.Now;
            ClosedDate = DateTime.Now.AddMonths(1);
        }

        static Bet()
        {
            DomainModelBase.RegisterType<Bet>();
        }

        #endregion

        #region Properties


        /// <summary>
        /// Id of user who made bet
        /// </summary>
        [XmlAttribute("BetId")]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int BetId { get; set; }

        /// <summary>
        /// Id of user who made bet
        /// </summary>
        [XmlAttribute("UserId")]
        [ForeignKey("User")]
        public Int32 UserId { get; set; }
        public User User { get; set; }

        /// <summary>
        /// Lot id on which the bet was made.
        /// </summary>
        [XmlAttribute("LotId")]
        public Int32 LotId { get; set; }

        /// <summary>
        /// Bet value in USD
        /// </summary>
        [XmlAttribute("BetValue")]
        public Decimal BetValue { get; set; }

        /// <summary>
        /// Bet status. There are three statuses:
        /// 1) Made - user made the bet
        /// 2) Broken - someone broke this bet
        /// 3) Won - bet is winning
        /// </summary>
        [XmlAttribute("Status")]
        public eBetStatusType Status { get; set; }

        /// <summary>
        /// Date and time when the bet was made.
        /// </summary>
        [XmlAttribute("OpenDate", DataType = "date")]
        public DateTime OpenDate { get; set; }

        /// <summary>
        /// Date and time when the bet was broken / it won
        /// </summary>
        [XmlAttribute("ClosedDate", DataType = "date")]
        public DateTime ClosedDate { get; set; }

        #endregion

        #region [IStorageEntity Implementation]

        public override bool IsKeyEquals(object key)
        {
            var instance = key as Bet;

            if (instance != null)
            {
                if (this.UserId == instance.UserId && this.BetId == instance.BetId)
                {
                    return true;
                }
            }
            return false;
        }

        public override void AssignTo(object obj)
        {
            if (obj is Bet)
            {
                var bet = obj as Bet;

                // bet.LotId = LotId;
                bet.UserId = UserId;
                bet.BetValue = BetValue;
                bet.Status = Status;
                bet.OpenDate = OpenDate;
                bet.ClosedDate = ClosedDate;

                return;
            }
            throw new NotSupportedException();
        }
        #endregion // [IStorageEntity Implementation]

        # region Implement IEquatable<T>

        public bool Equals(Bet other)
        {
            if (other == null)
            {
                return false;
            }
            // (this.LotId == other.LotId)                && 
            return (this.UserId == other.UserId) && (this.BetValue == other.BetValue)
                && (this.Status == other.Status) && (this.OpenDate == other.OpenDate)
                && (this.ClosedDate == other.ClosedDate);
        }

        /// <summary>
        /// Override GetHashCode method
        /// </summary>
        /// <returns>Hash Code</returns>
        public override int GetHashCode()
        {
            return UserId ^ UserId;
        }

        /// <summary>
        /// Override Equals method
        /// </summary>
        /// <param name="b"><Bet>object</param>
        /// <returns></returns>
        public override bool Equals(object b)
        {
            if (b == null)
            {
                return false;
            }

            Bet bet = b as Bet;
            //(this.LotId == bet.LotId)
            //   &&
            return (this.UserId == bet.UserId) && (this.BetValue == bet.BetValue)
                && (this.Status == bet.Status) && (this.OpenDate == bet.OpenDate)
                && (this.ClosedDate == bet.ClosedDate);
        }

        #endregion
    }
}
