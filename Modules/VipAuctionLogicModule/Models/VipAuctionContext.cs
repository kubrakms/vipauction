﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ocorola.Module.Infrastructure.Abstractions;

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Models
{
    public class VipAuctionContext : DbContext
    {
        #region [Constructors]

        private VipAuctionContext()
            : base("DefaultConnection")
        {

        }

        #endregion // [Constructors]

        #region [Properties]

        public static VipAuctionContext Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (mInstance == null)
                    {
                        mInstance = new VipAuctionContext();
                    }
                    return mInstance;
                }
            }
        }

        private DbSet<User> Users { get; set; }
        private DbSet<UserProfile> UsersProfile { get; set; }
        private DbSet<Lot> Lots { get; set; }
        private DbSet<Bet> Bets { get; set; }
        private DbSet<UserHistory> UserHistories { get; set; }

        #endregion // [Properties]

        #region [Fields]

        private static readonly object __mutex = new object();
        private static  VipAuctionContext mInstance = default(VipAuctionContext);

        #endregion // [Fields]

    }
}
