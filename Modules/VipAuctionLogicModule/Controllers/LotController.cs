﻿// <copyright file="LotController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Module.Infrastructure;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Controllers
{
    internal class LotController : ILotContract
    {
        #region [Constructors/Finalizers]

        public LotController()
        {
            lotRepository = AuctionLogicContainer.Instance.GetService<IRepository<Lot>>();
        }

        ~LotController()
        {
            Dispose(true);
        }

        #endregion  [Constructors/Finalizers]

        #region [ILotContract Implementation]

        /// <summary>
        /// returns enumeration of all lots with state marked as published which names include specified text 
        /// </summary>
        /// <param name="searchedText">text to search in names of available lots</param>
        /// <returns>enumeration of found lots</returns>
        public IEnumerable<Lot> GetAvailableLots(string searchedText = null)
        {
            return from lot in lotRepository.GetQuery()
                   where lot.State == eLotStateType.Published &&
                      (string.IsNullOrEmpty(searchedText) || lot.Name.Contains(searchedText))
                   select lot;
        }

        /// <summary>
        /// returns enumeration of all lots with state marked as NotActivated which names include specified text. These function is used by administrator
        /// </summary>
        /// <param name="searchedText">text to search in names of not activated lots</param>
        /// <returns>enumeration of found lots</returns>
        public IEnumerable<Lot> GetNotActivatedLots(string searchedText = null)
        {
            return from lot in lotRepository.GetQuery()
                   where lot.State == eLotStateType.NotActivated &&
                      (string.IsNullOrEmpty(searchedText) || lot.Name.Contains(searchedText))
                   select lot;

        }

        /// <summary>
        /// retrieves all lots marked as published and sorts it using standart Sort method of List with specified comparer
        /// </summary>
        /// <param name="comparer">Comparer to pass to List with specified condition of sorting</param>
        public void SortAvailableLots(IComparer<Lot> comparer)
        {
            List<Lot> listAvalibleLots = GetAvailableLots() as List<Lot>;

            if (listAvalibleLots != null)
            {
                listAvalibleLots.Sort(comparer);
            }

        }

        /// <summary>
        /// search and retrievs all lots published by user with specified name
        /// </summary>
        /// <param name="lotName">name of the owner of all lots to search</param>
        /// <returns>enumeration of all lots connected with specified user</returns>
        public IEnumerable<Lot> GetLotsByName(string lotName)
        {
            return from lot in lotRepository.GetQuery()
                   where lot.Name == lotName
                   select lot;
        }

        /// <summary>
        /// among the all lots of the user with specified id retrieves only lots wich name contain specified text.
        /// </summary>
        /// <param name="userId">unique id of the user who owns the lots to find</param>
        /// <param name="searchedText">text wich contains in names of lots to find</param>
        /// <returns></returns>
        public IEnumerable<Lot> GetUserLots(int userId, string searchedText = null)
        {
            return from lot in lotRepository.GetQuery()
                   where lot.UserId == userId &&
                      (string.IsNullOrEmpty(searchedText) || lot.Name.Contains(searchedText))
                   select lot;
        }

        /// <summary>
        /// find the lot with specified id
        /// </summary>
        /// <param name="lotId">unique id of required lot</param>
        /// <returns>the instance of found Lot. Returns default(Lot) if required lot wasn't found</returns>
        public Lot GetLot(int lotId)
        {
            return (from lot in lotRepository.GetQuery()
                    where lot.LotId == lotId
                    select lot).SingleOrDefault();
        }

        /// <summary>
        /// Saves passed lot into repository
        /// </summary>
        /// <param name="lot">instance of Lot which parameters will be assigned to a new lot in repository</param>
        public void CreateLot(Lot lot)
        {
            lotRepository.Add(lot);
        }

        /// <summary>
        /// retrievs the Lot with specified id and changes its status from NotActivated to Canceled.
        /// Then call Update method of the corresponding repository.
        /// </summary>
        /// <param name="lotId">unique identificator of lot to be canceled</param>
        public void CanselLot(int lotId)
        {
            var item = lotRepository.Item(lotId) as Lot;

            if (item == null)
                return;

            if (item.State == eLotStateType.NotActivated)
            {
                item.State = eLotStateType.Canceled;
                lotRepository.Update(item);
            }
        }

        /// <summary>
        /// retrievs the Lot with specified id and changes its status from NotActivated to Published.
        /// Then call Update method of the corresponding repository.
        /// These method is available only for administrator role.
        /// </summary>
        /// <param name="lotId">unique identificator of lot to be activated</param>
        public void ActivateLot(int lotId)
        {
            var item = lotRepository.Item(lotId) as Lot;

            if (item == null)
                return;

            if (item.State == eLotStateType.NotActivated)
            {
                item.State = eLotStateType.Published;
                lotRepository.Update(item);
            }

        }

        /// <summary>
        /// retrievs the Lot with specified id and changes its status to Blocked.
        /// Then call Update method of the corresponding repository.
        /// These method is available only for administrator role.
        /// </summary>
        /// <param name="lotId">unique identificator of lot to be blocked</param>
        public void BlockLot(int lotId)
        {
            var item = lotRepository.Item(lotId) as Lot;

            if (item == null)
                return;

            item.State = eLotStateType.Blocked;

            lotRepository.Update(item);
        }

        #endregion //[ILotContract Implementation]

        #region Disposeble implementation

        /// <summary>
        /// Dispose Lot Controller
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    lotRepository.Dispose();
                }
            }

            disposed = true;
        }

        #endregion

        #region [Fields]

        private bool disposed = false;

        private readonly IRepository<Lot> lotRepository = default(IRepository<Lot>);

        #endregion // [Fields]
    }
}
