﻿// <copyright file="BetController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Module.Infrastructure;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Controllers
{
    /// <summary>
    /// Represent bet behavior
    /// </summary>
    internal class BetController : IBetContract
    {
        #region [Constructors/Finalizers]

        public BetController()
        {
            betRepository = AuctionLogicContainer.Instance.GetService<IRepository<Bet>>();
        }

        ~BetController()
        {
            Dispose(true);
        }

        #endregion  [Constructors/Finalizers]

        #region [IBetContract Implementation]

        /// <summary>
        /// Get all bets for curent lot
        /// </summary>
        /// <param name="lotId">Lot Id</param>
        /// <returns>Colection of bets</returns>
        public IEnumerable<Bet> GetBetsByLot(int lotId)
        {
           return from bet in betRepository.GetQuery()
                         where bet.LotId == lotId
                         select bet;
        }

        /// <summary>
        /// Get all bets for curent user
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Colection of bets</returns>
        public IEnumerable<Bet> GetBetsByUser(int userId)
        {
           return from bet in betRepository.GetQuery()
                         where bet.UserId == userId
                         select bet;
        }

        /// <summary>
        /// Create a new bet
        /// </summary>
        /// <param name="bet">Bet</param>
        /// <returns>Id in data base</returns>
        public void MakeBet(Bet bet)
        {
            betRepository.Add(bet);
        }

        #endregion // [IBetContract Implementation]

        #region Disposeble implementation

        /// <summary>
        /// Dispose Bet Controller
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    betRepository.Dispose();
                }
            }

            disposed = true;
        }

        #endregion

        #region [Fields]

        private bool disposed = false;

        private readonly IRepository<Bet> betRepository = default(IRepository<Bet>);

        #endregion // [Fields]

    }
}
