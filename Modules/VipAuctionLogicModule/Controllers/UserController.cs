﻿// <copyright file="UserController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Module.Infrastructure;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Controllers
{
    internal class UserController : IUserContract
    {
        #region [Constructors/Finalizers]

        public UserController()
        {
            userProfileRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserProfile>>();
            userHistoryRepository = AuctionLogicContainer.Instance.GetService<IRepository<UserHistory>>();
        }

        ~UserController()
        {
            Dispose(true);
        }

        #endregion  [Constructors/Finalizers]

        #region [IUserContract Implementation]

        /// <summary>
        /// Saves passed user to the corresponding repository
        /// </summary>
        /// <param name="userProfile">UserProfile instance which parameter will be assigned to a new user in repository</param>
        public void CreateUser(UserProfile userProfile)
        {
            userProfileRepository.Add(userProfile);
        }

        /// <summary>
        /// Retrievs the enumeration of users from the corresponding repository
        /// which first names or last names or logins contains the specified text.
        /// If the text is not passed, returns all users.
        /// </summary>
        /// <param name="searchedText">The text which will be searched in the properties listed above</param>
        /// <returns>Enumeration of UserProfile instances of found users</returns>
        public IEnumerable<UserProfile> ViewUsers(string searchedText = null)
        {
            return from userProfile in userProfileRepository.GetQuery()
                         where string.IsNullOrEmpty(searchedText) || userProfile.FirstName.Contains(searchedText)
                            || userProfile.LastName.Contains(searchedText)
                            || userProfile.UserName.Contains(searchedText)
                         select userProfile;
        }

        /// <summary>
        /// Retrives the user from the corresponding repository with specified id
        /// </summary>
        /// <param name="userId">unique identificator of the requierd user</param>
        /// <returns>UserProfile instance of the user retrived by specified id</returns>
        public UserProfile GetProfile(int userId)
        {
            return (UserProfile)userProfileRepository.Item(userId);
        }

        /// <summary>
        /// Calls GetProfile method to retrive needed user by specified id from users repository 
        /// Then changes his status to blocked or unblocked depending on parameter needBlock.
        /// Then updates corresponding repository. 
        /// This method is available only for administrator's role
        /// </summary>
        /// <param name="userId">the unique identificator of user to be block or unblock</param>
        /// <param name="needBlock">the state of user which means if it will be blocked or unblocked after this method's invocation</param>
        public void BlockUser(int userId, bool needBlock)
        {
            var tempUser = GetProfile(userId);

            if (tempUser != null)
            {
                tempUser.IsBlocked = needBlock;
                userProfileRepository.Update(tempUser);
            }
        }

        /// <summary>
        /// Calls GetProfile method to retrive needed user by specified id from users repository 
        /// Then adds the specified value to his ballance.
        /// Then updates corresponding repository. 
        /// </summary>
        /// <param name="userId">the unique identificator of user which ballance will be changed</param>
        /// <param name="money">the value wich will be added to current user's ballance</param>
        public void ChangeUserBalance(int userId, decimal money)
        {
            var tempUser = GetProfile(userId);

            if (tempUser != null)
            {
                tempUser.Balance = tempUser.Balance + money;
                userProfileRepository.Update(tempUser);
            }
        }

        /// <summary>
        /// Save all changed parameters of passed user to the repository
        /// </summary>
        /// <param name="profile">UserProfile instance needed for saving</param>
        public void SaveProfile(UserProfile profile)
        {
            userProfileRepository.Update(profile);
        }

        #endregion //[IUserContract Implementation]

        #region Disposeble implementation

        /// <summary>
        /// Dispose User Controller
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    userProfileRepository.Dispose();
                    userHistoryRepository.Dispose();
                }
            }

            disposed = true;
        }

        #endregion

        #region [Fields]

        private bool disposed = false;

        private readonly IRepository<UserProfile> userProfileRepository = default(IRepository<UserProfile>);
        private readonly IRepository<UserHistory> userHistoryRepository = default(IRepository<UserHistory>);

        #endregion // [Fields]
    }
}
