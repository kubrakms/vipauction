﻿// <copyright file="AuctionController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Team1</author>

using System;
using System.Collections.Generic;
using Ocorola.Module.Infrastructure;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Repositories;
using System.Linq;


namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Controllers
{
    /// <summary>
    /// Represents  methods for context About information
    /// </summary>
    public class AboutController : IAboutContract
    {
        #region [Constructors/Finalizers]

        public AboutController()
        {
            repository = AuctionLogicContainer.Instance.GetService<IRepository<AboutInformation>>(new object[] { "XmlBase"});
        }

        ~AboutController()
        {
            Dispose(false);
        }

        #endregion  [Constructors/Finalizers]

        #region [IAuctionContract implementation]

        /// <summary>
        /// Get main image for About Box
        /// </summary>
        /// <returns>Path to image</returns>
        public string GetAboutImageSource()
        {
            return repository.Enumerate().FirstOrDefault().AboutImageSource;
        }

        /// <summary>
        /// Get the About information text
        /// </summary>
        /// <returns>String text</returns>
        public string GetAboutText()
        {
            return repository.Enumerate().FirstOrDefault().AboutText;
        }

        /// <summary>
        /// Get the Copyright text
        /// </summary>
        /// <returns>String text copyright</returns>
        public string GetCopyright()
        {
            return repository.Enumerate().FirstOrDefault().CopyRight;
        }

        /// <summary>
        /// Get the contacts informations
        /// </summary>
        /// <returns>String contacts information</returns>
        public string GetContacts()
        {
            return repository.Enumerate().FirstOrDefault().Contacts;
        }

        /// <summary>
        /// Get the developers information
        /// </summary>
        /// <returns>Enumerable information obout developers</returns>
        public IEnumerable<Models.DevelopersInfo> GetDevelopers()
        {
            return repository.Enumerate().FirstOrDefault().Develpers;
        }

        /// <summary>
        /// Set the about storage file
        /// </summary>
        /// <param name="filePath">file path</param>
        public void SetFilePath(string filePath)
        {
            if (repository.Enumerate().Count()==0)
            {
                var initdata = repository.Create();
                initdata.DefaultFileName = filePath;
                repository.Add(initdata);
            }
            
            //repository.Enumerate().FirstOrDefault().DefaultFileName = filePath;
        }
        
        /// <summary>
        /// Dispose About Controller
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {

                }
            }

            disposed = true;
        }

        #endregion // [IAuctionContract implementation]

        #region [Fields]

        private bool disposed = false;

        private readonly IRepository<AboutInformation> repository = default(IRepository<AboutInformation>);

        #endregion // [Fields]
    }
}