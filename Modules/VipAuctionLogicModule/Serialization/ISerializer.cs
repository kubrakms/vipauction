﻿// -----------------------------------------------------------------------
// <copyright file="ISerializer.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;


    /// <summary>
    /// Represent Logic of Serialization
    /// </summary>
    public interface ISerializer
    {
        void Serialize(AboutInformation data, string fileName);
        AboutInformation Deserialize(string fileName);

    }
}
