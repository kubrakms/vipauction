﻿// -----------------------------------------------------------------------
// <copyright file="XmlSerializer.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.VipAuction.Modules.VipAuctionServerModule.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Xml.Serialization;
    using System.Xml;
    using System.Runtime.Serialization;
    using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;

    /// <summary>
    /// Implement ISerializer logic for xml format
    /// </summary>
    public class XMLSerializer : ISerializer
    {
        public void Serialize(AboutInformation data, string fileName)
        {
            XmlSerializer xs = new XmlSerializer(typeof(AboutInformation), new XmlRootAttribute("AuctionInformation"));
            using (XmlWriter xw = XmlWriter.Create(fileName))
            {
                xs.Serialize(xw, data);
            }
        }



        public AboutInformation Deserialize(string fileName)
        {
            AboutInformation about = default(AboutInformation);
            XmlSerializer xs = new XmlSerializer(typeof(AboutInformation), new XmlRootAttribute("AuctionInformation"));
            using (FileStream readFileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                about = (AboutInformation)xs.Deserialize(readFileStream);
            }
            return about;
        }

        public void Serialize(AboutInformation data, Stream stream)
        {
            XmlSerializer xs = new XmlSerializer(typeof(AboutInformation), new XmlRootAttribute("AuctionInformation"));

            xs.Serialize(stream, data);

        }

        public object Deserialize(Stream stream)
        {
            AboutInformation about = default(AboutInformation);
            XmlSerializer xs = new XmlSerializer(typeof(AboutInformation), new XmlRootAttribute("AuctionInformation"));

            about = (AboutInformation)xs.Deserialize(stream);

            return about;
        }
    }
}
