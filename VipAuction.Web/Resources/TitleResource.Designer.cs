﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewRes {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class TitleResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TitleResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SoftServe.VipAuction.Web.Resources.TitleResource", typeof(TitleResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to List of lots to approve.
        /// </summary>
        public static string ApproveLotsWindowTitle {
            get {
                return ResourceManager.GetString("ApproveLotsWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create New Lot.
        /// </summary>
        public static string CreateLotWindowTitle {
            get {
                return ResourceManager.GetString("CreateLotWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No.
        /// </summary>
        public static string False {
            get {
                return ResourceManager.GetString("False", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forgot your password?.
        /// </summary>
        public static string ForgotPassword {
            get {
                return ResourceManager.GetString("ForgotPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Bets.
        /// </summary>
        public static string MyBetsWindowTitle {
            get {
                return ResourceManager.GetString("MyBetsWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Profile.
        /// </summary>
        public static string MyProfileWindowTitle {
            get {
                return ResourceManager.GetString("MyProfileWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recent Lots.
        /// </summary>
        public static string RecentLotsTitle {
            get {
                return ResourceManager.GetString("RecentLotsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Required Fields.
        /// </summary>
        public static string RequiredFieldText {
            get {
                return ResourceManager.GetString("RequiredFieldText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        public static string SearchTitle {
            get {
                return ResourceManager.GetString("SearchTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to tel.
        /// </summary>
        public static string TelephoneText {
            get {
                return ResourceManager.GetString("TelephoneText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes.
        /// </summary>
        public static string True {
            get {
                return ResourceManager.GetString("True", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I have read the user agreement.
        /// </summary>
        public static string UserAgreementConfirm {
            get {
                return ResourceManager.GetString("UserAgreementConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By accepting this Agreement or by visiting pages, User automatically agrees to the terms of the Confidentiality Agreement regarding
        ///
        ///the use of personalized and non-personalized confidential information, respectively..
        /// </summary>
        public static string UserAgreementText {
            get {
                return ResourceManager.GetString("UserAgreementText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Agreement.
        /// </summary>
        public static string UserAgreementTitle {
            get {
                return ResourceManager.GetString("UserAgreementTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register New User.
        /// </summary>
        public static string UserRegistrationWindowTitle {
            get {
                return ResourceManager.GetString("UserRegistrationWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Users.
        /// </summary>
        public static string UsersWindowTitle {
            get {
                return ResourceManager.GetString("UsersWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View Lots.
        /// </summary>
        public static string ViewLotsWindowTitle {
            get {
                return ResourceManager.GetString("ViewLotsWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View Lot.
        /// </summary>
        public static string ViewLotWindowTitle {
            get {
                return ResourceManager.GetString("ViewLotWindowTitle", resourceCulture);
            }
        }
    }
}
