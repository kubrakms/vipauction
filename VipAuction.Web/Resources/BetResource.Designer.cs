﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewRes {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class BetResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal BetResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SoftServe.VipAuction.Web.Resources.BetResource", typeof(BetResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Broken.
        /// </summary>
        public static string BetStatusBroken {
            get {
                return ResourceManager.GetString("BetStatusBroken", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Made.
        /// </summary>
        public static string BetStatusMade {
            get {
                return ResourceManager.GetString("BetStatusMade", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Won.
        /// </summary>
        public static string BetStatusWon {
            get {
                return ResourceManager.GetString("BetStatusWon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bet.
        /// </summary>
        public static string BetTitle {
            get {
                return ResourceManager.GetString("BetTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value.
        /// </summary>
        public static string BetValue {
            get {
                return ResourceManager.GetString("BetValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Closed.
        /// </summary>
        public static string ClosedDate {
            get {
                return ResourceManager.GetString("ClosedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Id.
        /// </summary>
        public static string Id {
            get {
                return ResourceManager.GetString("Id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lot Id.
        /// </summary>
        public static string LotId {
            get {
                return ResourceManager.GetString("LotId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lot.
        /// </summary>
        public static string LotName {
            get {
                return ResourceManager.GetString("LotName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opened.
        /// </summary>
        public static string OpenDate {
            get {
                return ResourceManager.GetString("OpenDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Id.
        /// </summary>
        public static string UserId {
            get {
                return ResourceManager.GetString("UserId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User.
        /// </summary>
        public static string UserName {
            get {
                return ResourceManager.GetString("UserName", resourceCulture);
            }
        }
    }
}
