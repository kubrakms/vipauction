﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewRes {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LotResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LotResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SoftServe.VipAuction.Web.Resources.LotResource", typeof(LotResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current Bet.
        /// </summary>
        public static string CurrentBet {
            get {
                return ResourceManager.GetString("CurrentBet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End Date.
        /// </summary>
        public static string EndDateTime {
            get {
                return ResourceManager.GetString("EndDateTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Id.
        /// </summary>
        public static string Id {
            get {
                return ResourceManager.GetString("Id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Bet User Id.
        /// </summary>
        public static string LastBetUserId {
            get {
                return ResourceManager.GetString("LastBetUserId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Bet User.
        /// </summary>
        public static string LastBetUserName {
            get {
                return ResourceManager.GetString("LastBetUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blocked.
        /// </summary>
        public static string LotStateBlocked {
            get {
                return ResourceManager.GetString("LotStateBlocked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancelled.
        /// </summary>
        public static string LotStateCancelled {
            get {
                return ResourceManager.GetString("LotStateCancelled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Closed.
        /// </summary>
        public static string LotStateClosed {
            get {
                return ResourceManager.GetString("LotStateClosed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not Activated.
        /// </summary>
        public static string LotStateNotActivated {
            get {
                return ResourceManager.GetString("LotStateNotActivated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sold.
        /// </summary>
        public static string LotStateSold {
            get {
                return ResourceManager.GetString("LotStateSold", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lot.
        /// </summary>
        public static string LotTitle {
            get {
                return ResourceManager.GetString("LotTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Owner.
        /// </summary>
        public static string OwnerName {
            get {
                return ResourceManager.GetString("OwnerName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price.
        /// </summary>
        public static string StartingPrice {
            get {
                return ResourceManager.GetString("StartingPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to State.
        /// </summary>
        public static string State {
            get {
                return ResourceManager.GetString("State", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Id.
        /// </summary>
        public static string UserId {
            get {
                return ResourceManager.GetString("UserId", resourceCulture);
            }
        }
    }
}
