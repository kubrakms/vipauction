﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace SoftServe.VipAuction.Web.Filters
{
    /// <summary>
    /// Custom error filter.
    /// If no named parameters were set code 500 will be returned and redirected to error 500 page.
    /// If the HttpException occurs than custom StatusCode and View are ignored.
    /// In other cases you can set custom redirection view, status code and specify the exception to be filtered.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    public class CustomExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// Get or Set filtering the exact Exception 
        /// </summary>
        public Type ExceptionType
        {
            get
            {
                return exceptionType;
            }
            set
            {
                if (value == null)
                {
                    exceptionType = typeof(Exception);
                }
                else
                    exceptionType = value;
            }
        }

        /// <summary>
        /// Get or set returning status code
        /// </summary>
        public int StatusCode
        {
            get
            {
                return statusCode;
            }
            set
            {
                statusCode = value;
            }
        }

        /// <summary>
        /// Get or set redirection view
        /// </summary>
        public string View
        {
            get
            {
                return viewName;
            }
            set
            {
                viewName = value;
            }
        }

        Type exceptionType = typeof(Exception);
        int statusCode = -1;
        string viewName = "";

        public virtual void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled) return;
            if (!filterContext.HttpContext.IsCustomErrorEnabled) return;
            if (filterContext.Exception == null) return;

            var exception = filterContext.Exception;
            if (exception is TargetInvocationException)
                exception = (exception as TargetInvocationException).InnerException;
            if (!exceptionType.IsInstanceOfType(exception)) return; //it's not our exception

            filterContext.Result = new ViewResult
            {
                ViewData = filterContext.Controller.ViewData,
                TempData = filterContext.Controller.TempData/*,
                ViewName = viewName*/
            };

            if (exception is HttpException)
            {
                var httpEx = exception as HttpException;

                switch (httpEx.GetHttpCode())
                {
                    case 404:
                        View = ConfigurationManager.AppSettings["Error404View"];
                        statusCode = 404;
                        break;

                    case 400:
                        View = ConfigurationManager.AppSettings["Error400View"];
                        statusCode = 400;
                        break;

                    case 401:
                        View = ConfigurationManager.AppSettings["Error401View"];
                        statusCode = 401;
                        break;

                    case 501:
                        View = ConfigurationManager.AppSettings["Error501View"];
                        statusCode = 501;
                        break;

                    case 500:
                        View = ConfigurationManager.AppSettings["Error500View"];
                        statusCode = 500;
                        break;
                    // others if any
                    default:
                        View = ConfigurationManager.AppSettings["Error500View"];
                        statusCode = 500;
                        break;
                }
            }
            if (string.IsNullOrEmpty(View))
                View = ConfigurationManager.AppSettings["Error500View"];

            if (StatusCode < 0)
                statusCode = 500;

            (filterContext.Result as ViewResult).ViewName = ConfigurationManager.AppSettings["ErrorViewDirectory"] + "/" + View + ".cshtml";
            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = statusCode;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

            //Template method, override this in inherited class to execute custom logic
            //for this exception
            OnExceptionHandled(filterContext);
        }

        //Override this to add post-processing specific to the exception thrown
        protected virtual void OnExceptionHandled(ExceptionContext filterContext)
        { }
    }
}