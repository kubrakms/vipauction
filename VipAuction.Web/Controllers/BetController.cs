﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Ocorola.Module.Infrastructure;

using Kendo.Mvc.Extensions;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule;
using SoftServe.VipAuction.Web.Helpers;
using SoftServe.VipAuction.Web.Models;

namespace SoftServe.VipAuction.Web.Controllers
{
    public class BetController : Controller
    {

        private IBetContract mDataController = AuctionLogicContainer.Instance.GetService<IBetContract>();

        public ActionResult Index()
        {
            return View();
        }


        [AcceptVerbs("POST")]
        public JsonResult GetBetsByLot(int id, [DataSourceRequest] DataSourceRequest request)
        {
            var bets = ViewModelsMapper.Map<IEnumerable<Bet>, IEnumerable<BetViewModel>>(mDataController.GetBetsByLot(id));

            if (bets == null)
                bets = new BetViewModel[] { };

            return Json(bets.ToDataSourceResult(request));
        }

        [AcceptVerbs("POST")]
        public decimal MakeBet(AddBetViewModel bet)
        {
            var newBet = ViewModelsMapper.Map<AddBetViewModel, Bet>(bet);

            if (newBet == null) return -1;

            newBet.OpenDate = DateTime.Now;
            newBet.UserId = 1;
            mDataController.MakeBet(newBet);

            return bet.BetValue;
        }
    }
}
