﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Ocorola.Module.Infrastructure;

using Kendo.Mvc.Extensions;

using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule;
using SoftServe.VipAuction.Web.Helpers;
using SoftServe.VipAuction.Web.Models;

namespace SoftServe.VipAuction.Web.Controllers
{
    public class UserController : Controller
    {
        private IUserContract mDataController = AuctionLogicContainer.Instance.GetService<IUserContract>();

        public ActionResult MyProfile(int id)
        {
            if (id <= 0) id = (int)Session["UserId"];

            var profile = ViewModelsMapper.Map<UserProfile, UserProfileViewModel>(mDataController.GetProfile(id));
            return View(profile);
        }

        public JsonResult GetUsers(string searchedText, [DataSourceRequest] DataSourceRequest request)
        {
            var users = ViewModelsMapper.Map<IEnumerable<UserProfile>, IEnumerable<UserViewModel>>(mDataController.ViewUsers());

            if (users == null)
                users = new UserViewModel[] { };

            return Json(users.ToDataSourceResult(request));
        }

        [AcceptVerbs("POST")]
        public void UpdateUser(EditProfileViewModel user)
        {
            var profile = ViewModelsMapper.Map<EditProfileViewModel, UserProfile>(user);

            if (profile == null) return;
            mDataController.SaveProfile(profile);
        }

        
    }
}
