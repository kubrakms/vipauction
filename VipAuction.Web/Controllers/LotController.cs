﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Ocorola.Module.Infrastructure;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Enums;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Modules.VipAuctionServerModule;
using SoftServe.VipAuction.Web.Helpers;
using SoftServe.VipAuction.Web.Models;

namespace SoftServe.VipAuction.Web.Controllers
{
    public class LotController : Controller
    {
        private ILotContract mDataController = AuctionLogicContainer.Instance.GetService<ILotContract>();

        public ActionResult Index(string id)
        {
            if (string.Compare(id, "all", true) == 0)
                id = "-1";

            ViewBag.userId = int.Parse(id);

            return View();
        }

        public ActionResult ViewLot(int id, int role)
        {
            if (role == 0)
            {
                ViewBag.admin = false;

            }
            else
            {
                ViewBag.admin = true;
            }

            var lotView = ViewModelsMapper.Map<Lot, ViewLotViewModel>(mDataController.GetLot(id));

            return View(lotView);
        }

        public JsonResult GetLots(int id, string searchedText, [DataSourceRequest] DataSourceRequest request)
        {
            IEnumerable<Lot> lots = null;

            if(id>-1)
                lots = mDataController.GetUserLots(id, searchedText);
            else
                lots = mDataController.GetAvailableLots(searchedText);

            var lotsViewModel = ViewModelsMapper.Map<IEnumerable<Lot>, IEnumerable<CatalogViewModel>>(lots);

            if (lotsViewModel == null)
                lotsViewModel = new CatalogViewModel[] { };

            return Json(lotsViewModel.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs("POST")]
        public JsonResult GetLastLots([DataSourceRequest] DataSourceRequest request)
        {
            var lastLots = mDataController.GetAvailableLots().OrderByDescending(l => l.LotId).Take(3);
            var lastLotsViewModel = ViewModelsMapper.Map<IEnumerable<Lot>, IEnumerable<LastPublishedLotViewModel>>(lastLots);

            if (lastLotsViewModel == null)
                lastLotsViewModel = new LastPublishedLotViewModel[] { };

            return Json(lastLotsViewModel.ToDataSourceResult(request));
        }

        [AcceptVerbs("POST")]
        public void AddLot(CreateNewLotViewModel lot)
        {
            if ((int)Session["UserId"] == -1) return;
            Lot newLot = new Lot();

            ViewModelsMapper.Map<CreateNewLotViewModel, Lot>(lot, newLot);

            if (newLot == null) return;

            newLot.State = eLotStateType.NotActivated;
            newLot.UserId = (int)Session["UserId"];
            mDataController.CreateLot(newLot);
        }

        [AcceptVerbs("POST")]
        public JsonResult GetApproveLots(string searchedText, [DataSourceRequest] DataSourceRequest request)
        {
            var approveLots = mDataController.GetNotActivatedLots(searchedText);
            var approveLotsViewModel = ViewModelsMapper.Map<IEnumerable<Lot>, IEnumerable<ApproveLotViewModel>>(approveLots);

            if (approveLotsViewModel == null)
                approveLotsViewModel = new ApproveLotViewModel[] { };

            return Json(approveLotsViewModel.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs("POST")]
        public void ActivateLot(int lotId)
        {
            mDataController.ActivateLot(lotId);
        }

        [AcceptVerbs("POST")]
        public void ChangeLotState(ChangeLotStateViewModel lot)
        {
            switch ((eLotStateType)lot.State)
            {
                case eLotStateType.Blocked:
                    mDataController.BlockLot(lot.LotId);
                    break;
                case eLotStateType.Published:
                    mDataController.ActivateLot(lot.LotId);
                    break;
                case eLotStateType.Canceled:
                    mDataController.CanselLot(lot.LotId);
                    break;
                case eLotStateType.Sold:
                    break;
                case eLotStateType.Closed:
                    break;
                default:
                    break;
            }
        }
    }
}
