﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.VipAuction.Web.Controllers
{
    /// <summary>
    /// Views error pages
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Error 400 page
        /// </summary>
        public ActionResult Error400()
        {
            return View();
        }

        /// <summary>
        /// Error 401 page
        /// </summary>
        public ActionResult Error401()
        {
            return View();
        }

        /// <summary>
        /// Error 404 page
        /// </summary>
        public ActionResult Error404()
        {
            return View();
        }

        /// <summary>
        /// Error 500 page
        /// </summary>
        public ActionResult Error500()
        {
            return View();
        }

        /// <summary>
        /// Error 501 page
        /// </summary>
        public ActionResult Error501()
        {
            return View();
        }

        /// <summary>
        /// Default error page
        /// </summary>
        public ActionResult DefaultErrorPage()
        {
            return View();
        }
    }
}
