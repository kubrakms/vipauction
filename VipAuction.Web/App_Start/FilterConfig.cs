﻿using System.Web;
using System.Web.Mvc;
using SoftServe.VipAuction.Web.Filters;

namespace SoftServe.VipAuction.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionFilterAttribute());
        }
    }
}