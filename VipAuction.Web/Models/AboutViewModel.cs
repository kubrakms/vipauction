﻿// <copyright file="AboutViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 03:33:49</date>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// View model for About window
    /// </summary>
    public class AboutViewModel
    {
        /// <summary>
        /// About image path
        /// </summary>
        public string AboutImageSource { get; set; }

        /// <summary>
        /// About Text
        /// </summary>
        public string AboutText { get; set; }

        /// <summary>
        /// Copyright text
        /// </summary>
        public string CopyRight { get; set; }

        /// <summary>
        /// Contacts
        /// </summary>
        public string Contacts { get; set; }
    }
}