﻿// <copyright file="BetViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 01:42:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// Bet view model
    /// </summary>
    public class BetViewModel
    {
        [DataType(DataType.DateTime)]
        public DateTime OpenDate { get; set; }

        [StringLength(20)]
        [DataType(DataType.Text)]
        public String UserName { get; set; }

        [DataType(DataType.Currency)]
        public Decimal BetValue { get; set; }
    }
}