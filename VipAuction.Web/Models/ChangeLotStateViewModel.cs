﻿// <copyright file="ChangeLotStateViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>01.04.2013 11:02:49</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.VipAuction.Web.Models
{
    public class ChangeLotStateViewModel
    {
        public Int32 LotId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Byte State { get; set; }
    }
}