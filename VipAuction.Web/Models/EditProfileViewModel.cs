﻿// <copyright file="EditProfileViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 02:58:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// Edit profile view model
    /// </summary>
    public class EditProfileViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public Int32 UserId { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String UserName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        [RegularExpression("[a-zA-Z]+")]
        public String FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        [RegularExpression("[a-zA-Z]+")]
        public String LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }
    }
}