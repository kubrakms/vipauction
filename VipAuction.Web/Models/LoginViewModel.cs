﻿// <copyright file="LoginViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 02:58:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// Login view model
    /// </summary>
    public class LoginViewModel
    {
        [Required]
        [StringLength(20, MinimumLength = 1)]
        public String Login { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Password)]
        public String Password { get; set; }
    }
}