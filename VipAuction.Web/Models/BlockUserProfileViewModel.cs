﻿// <copyright file="BlockUserProfileViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 02:58:49</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// View model for user blocking
    /// </summary>
    public class BlockUserProfileViewModel
    {
        public Int32 UserId { get; set; }

        public Boolean IsBlocked { get; set; }
    }
}