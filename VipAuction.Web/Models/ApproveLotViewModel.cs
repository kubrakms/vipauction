﻿// <copyright file="ApproveLotViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 02:20:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// View model for lot approving
    /// </summary>
    public class ApproveLotViewModel
    {
        public Int32 LotId { get; set; }

        [StringLength(50, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String Name { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String OwnerName { get; set; }

        [DataType(DataType.Currency)]
        public Decimal StartingPrice { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime EndDateTime { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Byte State { get; set; }
    }
}