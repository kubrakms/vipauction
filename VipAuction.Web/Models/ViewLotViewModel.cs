﻿// <copyright file="ViewLotViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 01:28:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// View model for lot viewing
    /// </summary>
    public class ViewLotViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public Int32 LotId { get; set; }

        [StringLength(50, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String Name { get; set; }

        [DataType(DataType.Currency)]
        public Decimal StartingPrice { get; set; }

        [DataType(DataType.Currency)]
        public Decimal CurrentBet { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String OwnerName { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime EndDateTime { get; set; }

        [StringLength(500)]
        [DataType(DataType.MultilineText)]
        public String Description { get; set; }
    }
}