﻿// <copyright file="UserProfileViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 02:32:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// User profile view model
    /// </summary>
    public class UserProfileViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public Int32 UserId { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String UserName { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        [RegularExpression("[a-zA-Z]+")]
        public String FirstName { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        [RegularExpression("[a-zA-Z]+")]
        public String LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        [DataType(DataType.Text)]
        public String Status { get; set; }

        public Boolean IsBlocked { get; set; }

        [DataType(DataType.Currency)]
        public Decimal Balance { get; set; }

        public Int32 PoinsByMonth { get; set; }

        public Int32 TotalPoints { get; set; }

        public Byte Discount { get; set; }

        public Int32 Experience { get; set; }
    }
}