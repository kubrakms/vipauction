﻿// <copyright file="DeveloperViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 03:38:49</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// Developer info view model
    /// </summary>
    public class DeveloperViewModel
    {
        /// <summary>
        /// Path to developers image
        /// </summary>
        public string ImageSource { get; set; }

        /// <summary>
        /// Some info about developer
        /// </summary>
        public string ShortInfo { get; set; }

        /// <summary>
        /// Developer FirstName 
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Developer LastName
        /// </summary>
        public string LastName { get; set; }
    }
}