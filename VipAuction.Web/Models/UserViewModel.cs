﻿// <copyright file="UserViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 02:21:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// User view model
    /// </summary>
    public class UserViewModel
    {
        public Int32 UserId { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String FirstName { get; set; }

        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String LastName { get; set; }

        public Int32 Experience { get; set; }

        public Byte Discount { get; set; }

        public Byte Status { get; set; }

        public Boolean IsBlocked { get; set; }
    }
}