﻿// <copyright file="RegisterUserViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 03:17:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// View model for user registration
    /// </summary>
    public class RegisterUserViewModel
    {
        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String UserName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String Password { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        [RegularExpression("[a-zA-Z]+")]
        public String FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        [DataType(DataType.Text)]
        [RegularExpression("[a-zA-Z]+")]
        public String LastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }
    }
}