﻿// <copyright file="CreateNewLotViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 01:21:49</date>

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// Create new lot view model
    /// </summary>
    public class CreateNewLotViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public String Name { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public Decimal StartingPrice { get; set; }

        [DataType(DataType.Date)]
        public DateTime EndDateTime { get; set; }

        [StringLength(500)]
        [DataType(DataType.MultilineText)]
        public String Description { get; set; }
    }
}