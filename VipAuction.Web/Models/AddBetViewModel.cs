﻿// <copyright file="AddBetViewModel.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>29.03.2013 01:51:49</date>


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.VipAuction.Web.Models
{
    /// <summary>
    /// View model for making bet
    /// </summary>
    public class AddBetViewModel
    {
        [Required]
        [HiddenInput(DisplayValue = false)]
        public Int32 LotId { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public Decimal BetValue { get; set; }
    }
}