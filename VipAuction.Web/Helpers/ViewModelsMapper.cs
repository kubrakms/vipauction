﻿// <copyright file="ViewModelsMapper.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Baboshkin Igor</author>
// <date>01.04.2013 00:19:49</date>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoftServe.VipAuction.Modules.VipAuctionServerModule;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Models;
using SoftServe.VipAuction.Modules.VipAuctionServerModule.Contracts;
using SoftServe.VipAuction.Web.Models;
using AutoMapper;

namespace SoftServe.VipAuction.Web.Helpers
{


    public static class ViewModelsMapper
    {
        private class LotOwnerNameResolver : ValueResolver<Lot, string>
        {
            IUserContract mUsers = AuctionLogicContainer.Instance.GetService<IUserContract>();

            protected override string ResolveCore(Lot source)
            {
                return mUsers.GetProfile(source.UserId).UserName;
            }
        }

        private class BetOwnerNameResolver : ValueResolver<Bet, string>
        {
            IUserContract mUsers = AuctionLogicContainer.Instance.GetService<IUserContract>();

            protected override string ResolveCore(Bet source)
            {
                return mUsers.GetProfile(source.UserId).UserName;
            }
        }

        static ViewModelsMapper()
        {
            Mapper.CreateMap<AddBetViewModel, Bet>();

            Mapper.CreateMap<Lot, ApproveLotViewModel>()
                .ForMember(dest => dest.OwnerName,
                    opt => opt.ResolveUsing<LotOwnerNameResolver>().ConstructedBy(() => new LotOwnerNameResolver()));

            Mapper.CreateMap<Bet, BetViewModel>()
                .ForMember(dest => dest.UserName,
                    opt => opt.ResolveUsing<BetOwnerNameResolver>().ConstructedBy(() => new BetOwnerNameResolver()));

            Mapper.CreateMap<UserProfile, BlockUserProfileViewModel>();

            Mapper.CreateMap<Lot, CatalogViewModel>()
                .ForMember(dest => dest.OwnerName,
                    opt => opt.ResolveUsing<LotOwnerNameResolver>().ConstructedBy(() => new LotOwnerNameResolver()));

            Mapper.CreateMap<CreateNewLotViewModel, Lot>();

            Mapper.CreateMap<EditProfileViewModel, UserProfile>();

            Mapper.CreateMap<Lot, LastPublishedLotViewModel>()
                .ForMember(dest => dest.OwnerName,
                    opt => opt.ResolveUsing<LotOwnerNameResolver>().ConstructedBy(() => new LotOwnerNameResolver()));

            Mapper.CreateMap<RegisterUserViewModel, UserProfile>();

            Mapper.CreateMap<UserProfile, UserProfileViewModel>();

            Mapper.CreateMap<UserProfile, UserViewModel>();

            Mapper.CreateMap<Lot, ViewLotViewModel>()
                .ForMember(dest => dest.OwnerName,
                    opt => opt.ResolveUsing<LotOwnerNameResolver>().ConstructedBy(() => new LotOwnerNameResolver()));

            Mapper.CreateMap<RegisterModel, UserProfile>();
        }

        public static TDestination Map<TSource, TDestination>(TSource source) where TDestination : class
        {
            if (source != null)
            {
                return Mapper.Map<TSource, TDestination>(source) as TDestination;
            }
            
            return null;
        }

        public static void Map<TSource, TDestination>(TSource source, TDestination destination) where TDestination : class
        {
            if (source != null)
            {
                Mapper.Map<TSource, TDestination>(source, destination);
            }
        }
    }
}