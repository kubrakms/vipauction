﻿var adminUserView = {

    searchedTextOnFocus: function (m) {

        var defaultValue = m;
        var value = $("#searchedText").attr("value");
        if (value == defaultValue) {
            $("#searchedText").attr("value", "");
        }

    },

    searchedTextLostFocus: function (m) {
        var defaultValue = m;
        var value = $("#searchedText").attr("value");
        if (value == "") {
            $("#searchedText").attr("value", defaultValue);
        }
    }, 

    showDetails: function (e) {

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $("#viewMyProfileWindow").kendoWindow({
            width: "400px",
            title: e.data.commandName.toString(),//"@ViewRes.TitleResource.UsersWindowTitle",
            draggable: true,
            height: "510px",
            scrollable: false,
            modal: true,
            content: "/User/MyProfile/" + dataItem.UserId + "?role=1",
            visible: false,
            close: $("#List_of_users").data("kendoGrid").dataSource.read(),
            resizable: false
        });
        $("#viewMyProfileWindow").data("kendoWindow").center().open();
    },

    gridChanged: function () {
        var height = $("#List_of_users").height();

        $("#gridMainDiv").height(height + 50);
    },

    AdminUserDetailsCloseClick: function () {
        $("#AdminUserDetails").data("kendoWindow").close();
    },

    onViewMyProfileWindowClose: function () {
        $("#List_of_users").data("kendoGrid").dataSource.read();
    },

    searchUsers: function (m) {
        var search = $("#searchedText").attr("value");
        var defaultValue = m;
        if (search == defaultValue) search = "";
        $("#List_of_users").data("kendoGrid").dataSource.transport.options.read.url = '/User/GetUsers/?searchedText=' + search;
        $("#List_of_users").data("kendoGrid").dataSource.read();
    }

};