﻿var lotView = {

    closeViewLotWindow: function () {
        $('#viewLotWindow').data('kendoWindow').close();
    },

    makeBetClick: function (lotId) {

        var url = "/Bet/MakeBet";
        var value = $("#txtMakeBet").attr("value");
        $.when(
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    "LotId": lotId,
                    "BetValue": value
                },
                dataType: "json"
            })
        ).done(
            function (data, textStatus, jqXHR) {
                //alert("OK");
                lotView.reloadBets(data);
            }
        );
    },

    reloadBets: function (value) {

        $("#LotBetsList").data("kendoGrid").dataSource.read();
        if (value > 0)
            $("#fcurrentprice").html(value);
    },

    changeLotState: function (state, lotId) {

        var url = "/Lot/ChangeLotState";
        $.when(
            $.ajax({
                url: url,
                type: "POST",
                data:
                    {
                        "LotId": lotId,
                        "State": state
                    },
                dataType: "json"
            })
        ).done(
            function (data, textStatus, jqXHR) {
            }
        );
    }

};