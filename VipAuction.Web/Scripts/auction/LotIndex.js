﻿var lotIndex = {

    searchLots: function (m, id) {
        var search = $("#searchedText").attr("value");
        var defaultValue = m;
        if (search == defaultValue) search = "";
        $("#ViewLotsGrid").data("kendoGrid").dataSource.transport.options.read.url = '/Lot/GetLots/' + id + '?searchedText=' + search;
        $("#ViewLotsGrid").data("kendoGrid").dataSource.read();
    },

    viewLotClick: function (e) {

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $("#viewLotWindow").kendoWindow({
            width: "640px",
            title: e.data.commandName.toString(),//"@ViewRes.TitleResource.ViewLotWindowTitle",
            draggable: true,
            height: "420px",
            scrollable: false,
            modal: true,
            content: "/Lot/ViewLot/" + dataItem.LotId + "?role=0",
            visible: false,
            resizable: false
        });
        $("#viewLotWindow").data("kendoWindow").center().open();
    },

    searchedTextOnFocus: function (text) {
        var defaultValue = text;
        var value = $("#searchedText").attr("value");
        if (value == defaultValue) {
            $("#searchedText").attr("value", "");
        }
    },

    searchedTextLostFocus: function (lostText) {
        var defaultValue = lostText;
        var value = $("#searchedText").attr("value");
        if (value == "") {
            $("#searchedText").attr("value", defaultValue);
        }
    },

    gridChanged: function () {

        var height = $("#ViewLotsGrid").height();

        $("#gridMainDiv").height(height + 50);
    },

    btnAddLotAddClick: function () {

        $('#AddNewLotWindow').data('kendoWindow').close()
        var url = '/Lot/AddLot';
        var value = $("#txtMakeBet").attr("value");
        $("#fcurrentprice").html(value);
        $.when(
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    "Name": $("#faddLotBlock_name").attr("value"),
                    "StartingPrice": $("#faddLotBlock_startingprice").attr("value"),
                    "Description": $("#addLotDescriptionText").val(),
                    EndDateTime: $("#faddLotBlock_endDate").attr("value"),
                    "Status": 1,
                    blocked: false
                },
                dataType: "json"
            })
        ).done(
            function (data, textStatus, jqXHR) {
                $("#ViewLotsGrid").data("kendoGrid").dataSource.read();
            }
        );

        $('#AddNewLotWindow').data('kendoWindow').close()
    },
    
    openAddLotWindow: function () {
        $('#AddNewLotWindow').data('kendoWindow').center().open()
    },

    closeAddLotWindow: function () {
        $('#AddNewLotWindow').data('kendoWindow').close();
    }
};

