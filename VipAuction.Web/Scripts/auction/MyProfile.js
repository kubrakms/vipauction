﻿var myProfile = {

    SetReadOnlyToFields: function (val) {
        $("#flogin").prop("readonly", val);
        $("#ffirstname").attr("readonly", val);
        $("#flasttname").attr("readonly", val);
        $("#femail").attr("readonly", val);
    },

    btnEditClick: function (saveButton, cancelButton, editButton, closeButton, userId) {
        var val = $("#btnedit").attr("value");
        if (val == "Edit") {
            $("#btnedit").attr("value", saveButton);
            $("#btnclose").attr("value", cancelButton);

            myProfile.SetReadOnlyToFields(false);
        }
        else {
            $("#btnedit").attr("value", editButton);
            $("#btnclose").attr("value", closeButton);
            myProfile.SetReadOnlyToFields(true);

            $("#floginTemp").attr("value", $("#flogin").attr("value"));
            $("#ffirstnameTemp").attr("value", $("#ffirstname").attr("value"));
            $("#flasttnameTemp").attr("value", $("#flasttname").attr("value"));
            $("#femailTemp").attr("value", $("#femail").attr("value"));

            myProfile.SaveUserData(userId);
        }
    },

    SaveUserData: function (userId) {
        $.ajax({
            url: '/User/UpdateUser/',
            type: "POST",
            data: {
                "UserId": userId,
                "UserName": $("#floginTemp").attr("value"),
                "FirstName": $("#ffirstnameTemp").attr("value"),
                "LastName": $("#flasttnameTemp").attr("value"),
                "Email": $("#femailTemp").attr("value"),
                "IsBlocked": $("#fblocked").html()
            },
            dataType: "json"
        });
    },

    btnCLoseClick: function (editButton, closeButton, userId) {
       
        var val = $("#btnclose").attr("value");
        if (val == closeButton) {
            $("#viewMyProfileWindow").data("kendoWindow").close()
        }
        else {
            $("#btnedit").attr("value", editButton);
            $("#btnclose").attr("value", closeButton);
            $("#flogin").attr("value", $("#floginTemp").attr("value"));
            $("#ffirstname").attr("value", $("#ffirstnameTemp").attr("value"));
            $("#flasttname").attr("value", $("#flasttnameTemp").attr("value"));
            $("#femail").attr("value", $("#femailTemp").attr("value"));
            myProfile.SetReadOnlyToFields(true);
            myProfile.SaveUserData(userId);
        }
    },

    btnBlockClick: function (blockUser, unblockUser, userId, userName, firstName, lastName, email) {
       
        var val = $("#AdminUserDetails_block").attr("value");
        var block = blockUser;
        var unblock = unblockUser;
        if (val == block) {
            $("#AdminUserDetails_block").attr("value", unblock);
            $("#fblocked").html("True");
        }
        else {
            $("#AdminUserDetails_block").attr("value", block);
            $("#fblocked").html("False");
        }

        $.ajax({
            url: 'User/UpdateUser/',
            type: "POST",
            data: {
                "UserId": userId,
                "UserName": userName,
                "FirstName": firstName,
                "LastName": lastName,
                "Email": email,
                "IsBlocked": $("#fblocked").html()
            },
            dataType: "json"
        });
    }
};