﻿var adminAproveLotsView = {

    searchedTextOnFocus: function (mes) {

        var defaultValue = mes;
        var value = $("#searchedText").attr("value");
        if (value == defaultValue) {
            $("#searchedText").attr("value", "");
        }
    },

    searchedTextLostFocus: function (mes) {

        var defaultValue = mes;
        var value = $("#searchedText").attr("value");
        if (value == "") {
            $("#searchedText").attr("value", defaultValue);
        }
    },

    gridChanged: function () {

        var height = $("#List_of_lots").height();

        $("#gridMainDiv").height(height + 50);
    },

    showDetails: function (e) {

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $("#viewLotWindow").kendoWindow({
            width: "440px",
            title: e.data.commandName.toString(),//"View Lot",
            draggable: true,
            height: "410px",
            scrollable: false,
            modal: true,
            content: "/Lot/ViewLot/" + dataItem.LotId + "?role=1",
            visible: false,
            resizable: false
        });
        $("#viewLotWindow").data("kendoWindow").center().open();
    },

    AdminApproveLotDetailsCloseClick: function () {
        $("#AdminApproveLotDetails").data("kendoWindow").close();
    },

    searchLots: function (m) {
        
        var search = $("#searchedText").attr("value");
        var defaultValue = m;
        if (search == defaultValue) search = "";
        $("#List_of_lots").data("kendoGrid").dataSource.transport.options.read.url = '/Lot/GetApproveLots/?searchedText=' + search;
        $("#List_of_lots").data("kendoGrid").dataSource.read();
    }

};