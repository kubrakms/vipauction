﻿
var layout = kendo.observable({
    currentUserName: "",
    registerAndLoginShow: true,
    logoutShow: false
});

var registerViewModel = kendo.observable({
    isAcceptrules: false,
    errors: []
});

var loginViewModel = kendo.observable({
    errors: []
});


function changeHeader(result) {
    //console.log(result);
    if (result.id > -1) {
        if (result.role == 1) {
            $("#MenuAdminUsers").show();
            $("#MenuAdminLots").show();
        }
        layout.set('currentUserName', result.name);
        layout.set("registerAndLoginShow", false);
        layout.set("logoutShow", true);
    }
    else {
        layout.set("registerAndLoginShow", true);
        layout.set("logoutShow", false);
    }
}

$(document).ready(function () {
    
    $("#MenuAdminUsers").hide();
    $("#MenuAdminLots").hide();
    $.ajax({
        url: '/Account/GetUserSession',
        type: "POST",
        data: {
        },
        success: changeHeader,
        error: function (res) {
        },
        dataType: "json"
    });

    //$("#profile").hide();
    $("#registerUser").bind("click", function () {
        $("#Register_new_user").data("kendoWindow").center().open();
    });

    $("#loginUser").bind("click", function () {
        $("#Login_user").data("kendoWindow").center().open();
        // $("#loginUser").hide();
    });

    $("#Canceling").click(function () {
        $("#Login_user").data("kendoWindow").close();
    });

    $("#canceling").click(function () {
        $("#Register_new_user").data("kendoWindow").close();
        window.location.reload();
    });

    $("#aboutOk").click(function () {
        $("#About_Window").data("kendoWindow").close();
    });

    $("#aboutBtn").click(function () {
        $("#About_Window").data("kendoWindow").center().open();
    });




    function passwordConfirmed() {
        return ($("#password").attr("value") && $("#confirmPassword").attr("value") &&
            $("#password").attr("value") == $("#confirmPassword").attr("value"));
    }

    function firstLastNameValidation(value) {
        var re = new RegExp("[a-zA-Z]");
        if (value.match(re))
            return true;
        return false;
    }

    function emailValidation() {
        var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/i);

        if ($("#email").attr("value").match(emailRegex))
            return true;

        return false;
    }

    function passwordValidation() {
        return ($("#password").attr("value") && $("#confirmPassword").attr("value") &&
              $("#password").attr("value") == $("#confirmPassword").attr("value"));
    }

    function checkBoxValidation() {

        if ($("#acceptCheckbox").attr("checked"))
            return true;
        return false;
    }

    function validateForm() {
        var isValid = true;
        var errorMessages = [];

        //  alert($.session("lang"));
        //if ($.session("lang")=="ru")

        if ($("#canceling").html() == "Close") {
            if (!firstLastNameValidation($("#FirstName").attr("value"))) {
                errorMessages.push("FirstName is not valid");
                isValid = false;
            }
            if (!firstLastNameValidation($("#LastName").attr("value"))) {
                errorMessages.push("LastName is not valid");
                isValid = false;
            }

            if (!emailValidation()) {
                errorMessages.push("Email not valid.");
                isValid = false;
            }

            if (!passwordValidation()) {
                errorMessages.push("Password not equal");
                isValid = false;
            }

            if (!checkBoxValidation()) {
                errorMessages.push("You must accept convension.");
                isValid = false;
            }
        }
        else {
            if (!firstLastNameValidation($("#FirstName").attr("value"))) {
                errorMessages.push("Имя не корректно");
                isValid = false;
            }
            if (!firstLastNameValidation($("#LastName").attr("value"))) {
                errorMessages.push("Фамилия не корректна");
                isValid = false;
            }

            if (!emailValidation()) {
                errorMessages.push("Email не корректен.");
                isValid = false;
            }

            if (!passwordValidation()) {
                errorMessages.push("Пароль не подходит");
                isValid = false;
            }

            if (!checkBoxValidation()) {
                errorMessages.push("Вы должны принять соглашение.");
                isValid = false;
            }
        }
        if (isValid)
            registerViewModel.set("isAcceptrules", true);
        else
            registerViewModel.set("isAcceptrules", false);
        registerViewModel.set("errors", errorMessages);

    }


    function resetErrorInLoginView() {
        //console.log("enter to func");
        loginViewModel.set("errors", []);
    }



    $("#Login").focusout(validateForm);
    $("#FirstName").focusout(validateForm);
    $("#LastName").focusout(validateForm);
    $("#email").focusout(validateForm);
    $("#password").focusout(validateForm);
    $("#confirmPassword").focusout(validateForm);
    $("#acceptCheckbox").change(validateForm);

    $("#UserLogin").focusout(resetErrorInLoginView);
    $("#UserPassword").focusout(resetErrorInLoginView);

   


    $("#confirming").click(function () {
        registerViewModel.set("isAcceptrules", false);
        $.ajax({
            url: '/Account/Register',
            type: "POST",
            data: {
                "UserName": $("#Login").attr("value"),
                "FirstName": $("#FirstName").attr("value"),
                "LastName": $("#LastName").attr("value"),
                "BirthDate": $("#datepicker").attr("value"),
                "EMail": $("#email").attr("value"),
                "Password": $("#password").attr("value"),
                "ConfirmPassword": $("#confirmPassword").attr("value")
            },
            success: function (result) {
                //layout.set('currentUserName', result);
                //$("#Register_new_user").data("kendoWindow").close();
                var str = $("#congirmMessage").data('dat')
                $("#congirmMessage").text(str);
                //registerViewModel.set("isAcceptrules", false);
                //layout.set("registerAndLoginShow", false);
                //layout.set("logoutShow", true);
                //changeHeader(result);
                //location.reload();
                //window.location.reload();
            },
            error: function (res) {
                registerViewModel.set("errors", [res.responseText]);
                layout.set("registerAndLoginShow", true);
                layout.set("logoutShow", false);
            },
            dataType: "json"
        });
    });

    $("#Logining").click(function () {
        $.ajax({
            url: '/Account/Login',
            type: "POST",
            data: {
                "UserName": $("#UserLogin").attr("value"),
                "Password": $("#UserPassword").attr("value"),
                "RememberMe": false
            },
            success: function (result) {
                //layout.set('currentUserName', result);
                $("#Login_user").data("kendoWindow").close();
                //layout.set("registerAndLoginShow", false);
                //layout.set("logoutShow", true);
                //changeHeader(result);
                window.location.reload();
            },
            error: function (res) {
                loginViewModel.set("errors", [res.responseText]);
                layout.set("registerAndLoginShow", true);
                layout.set("logoutShow", false);
            },
            dataType: "json"
        });
    });

    $("#logout").click(function () {
        $.ajax({
            url: '/Account/LogOff',
            type: "POST",
            success: function (result) {
                //console.log(result);
                window.location = '/Home/Index';
            },
            error: function (res) {
                //alert(res);
                //layout.set("registerAndLoginShow", true);
                //layout.set("logoutShow", false);
                //layout.set("currentUserName", "");
                window.location = '/Home/Index';
            },
            dataType: "json"
        });
    });



    kendo.bind($("#layout"), layout);
    kendo.bind($("#Register_new_user"), registerViewModel);
    kendo.bind($("#Login_new_user"), loginViewModel);
});


function onRegistrationWindowOpen() {
    $("#Login").attr("value", "");
    $("#FirstName").attr("value", "");
    $("#LastName").attr("value", "");
    $("#datepicker").attr("value", "10/10/2011");
    $("#email").attr("value", "");
    $("#password").attr("value", "");
    $("#confirmPassword").attr("value", "");
    $("#agree").removeAttr("checked");
}

function onLoginWindowOpen() {
    $("#UserLogin").attr("value", "");
    $("#UserPassword").attr("value", "");
}

function onClose() {
    $("#registerUser").show();
    $("#loginUser").show();
}

function showMyProfile() {
    $("#viewMyProfileWindow").kendoWindow({
        width: "400px",
        title: "User Profile",
        draggable: true,
        height: "510px",
        scrollable: false,
        modal: true,
        content: '/User/MyProfile/-1',
        visible: false,
        resizable: false
    });
    $("#viewMyProfileWindow").data("kendoWindow").center().open();
};
