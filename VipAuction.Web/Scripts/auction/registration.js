﻿var regConfirm = {

    // Move window to center when its open
    openWindow: function () {
        var win = $("#RegisterConfirmation").data("kendoWindow");
        win.center();
    },

    // Redirect to home page when windows is close
    redirect: function () {
        window.location.replace('/');
    }
}